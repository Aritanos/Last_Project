﻿using UnityEngine;

[CreateAssetMenu(fileName = "SpritesContainer", menuName = "Sprites Container" , order = 2)]
public class SpritesContainer : ScriptableObject
{
    private string nj2k3k205 = "serrrw";
    
    [SerializeField] private Sprite _favoritesButtonSelected;
    [SerializeField] private Sprite _favoritesButtonDeselected;
    [SerializeField] private Sprite _subCategoryButtonSelected;
    [SerializeField] private Sprite _subCategoryButtonDeselected;
    [SerializeField] private Sprite _swiperImageSelected;
    [SerializeField] private Sprite _swiperImageDeselected;

    private void J882929()
    {
        nj2k3k205 = nj2k3k205 + '3';
    }
    
    public Sprite FavoritesButtonSpriteSelected => _favoritesButtonSelected;
    public Sprite FavoritesButtonSpriteDeselected => _favoritesButtonDeselected;
    public Sprite SubCategoryButtonSpriteSelected => _subCategoryButtonSelected;
    public Sprite SubCategoryButtonSpriteDeselected => _subCategoryButtonDeselected;
    public Sprite SwiperImageSelected => _swiperImageSelected;
    public Sprite SwiperImageDeselected => _swiperImageDeselected;
}