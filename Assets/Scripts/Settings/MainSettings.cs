using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MainSettings", menuName = "Main Settings", order = 1)]
public class MainSettings : ScriptableObject
{
    public Vector4 p54g43gt1 = new Vector4(0.19f, 0.5f, -3, 1);
    public float StartingTime => _startingTime;
    public Material ImageMaterial => _imageMaterial;
    
    [SerializeField] private float _startingTime;
    [SerializeField] private Material _imageMaterial;
}