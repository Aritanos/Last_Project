﻿using System;
using UnityEngine;
using UnityEngine.UI;

public static class PoppyModsTextureOffsetHelper
{
    private static readonly int _poppyModsOffsetControllerMainTextureToChange = Shader.PropertyToID("_MainTex");

    public static void SetOffset(Material material, bool isPlaceholder)
    {
        if (material != null && isPlaceholder)
        {
            material.SetTextureOffset(_poppyModsOffsetControllerMainTextureToChange, new Vector2(0.0f, 0f));
        }
        else
        {
            material.SetTextureOffset(_poppyModsOffsetControllerMainTextureToChange, new Vector2(0.012f, 0f));
        }
    }
}