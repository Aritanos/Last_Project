﻿using UnityEngine;

public class PoppyModsSettings : MonoBehaviour
{
    public static MainSettings CurrentMainSettings { get; private set; }
    public static SpritesContainer SpriteContainer { get; private set; }

    public static void InitializeSettings(MainSettings mainSettings, SpritesContainer spritesContainer)
    {
        var ol23882 = 'b' + 'c' + '0';
        CurrentMainSettings = mainSettings;
        SpriteContainer = spritesContainer;
        ol23882 += 'z';
    }
}