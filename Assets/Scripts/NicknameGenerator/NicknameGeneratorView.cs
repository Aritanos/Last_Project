﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using IOSBridge;

public class NicknameGeneratorView : MonoBehaviour
{
    [SerializeField] private GameObject _poppyModsNicknameGeneratorTitle;
    [SerializeField] private TMP_Text _generatedNameText;
    [SerializeField] private TMP_Text _inscription;
    [SerializeField] private Image _poppyModsGeneratorBlurMask;
    [SerializeField] private LoadingImage _poppyModsNicknameGeneratorLoadingImage;

    public void SetGeneratorOpen()
    {
        _poppyModsNicknameGeneratorTitle.SetActive(true);
        _generatedNameText.gameObject.SetActive(false);
        _poppyModsGeneratorBlurMask.enabled = false;
        _poppyModsNicknameGeneratorLoadingImage.PoppyModsSetActive(false);
        _inscription.gameObject.SetActive(true);
    }

    public void SetGenerationStartState()
    {
        _poppyModsGeneratorBlurMask.enabled = true;
        _inscription.gameObject.SetActive(true);
        _poppyModsNicknameGeneratorLoadingImage.PoppyModsSetActive(true);
    }
    
    public void SetGenerationEndState(string generatedName)
    {
        _poppyModsGeneratorBlurMask.enabled = false;
        _poppyModsNicknameGeneratorLoadingImage.PoppyModsSetActive(false);
        _poppyModsNicknameGeneratorTitle.SetActive(false);
        _inscription.gameObject.SetActive(false);
        _generatedNameText.text = generatedName;
        _generatedNameText.gameObject.SetActive(true);
    }

    public void PoppyModsGeneratorCopyNickname()
    {
        #if UNITY_EDITOR
        Debug.Log("Nickname copied: " + _generatedNameText.text);
        #elif UNITY_IOS
        IOStoUnityBridge.ShowAlert("Nickname copied: ", _generatedNameText.text);
        IOStoUnityBridge.CopyText(_generatedNameText.text);
        #endif
    }
}