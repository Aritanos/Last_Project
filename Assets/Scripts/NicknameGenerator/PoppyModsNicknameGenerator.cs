using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class PoppyModsNicknameGenerator : MonoBehaviour, IActivatable
{
    [SerializeField] private float _generationTime;
    [SerializeField] private NicknameGeneratorView _nicknameGeneratorView;
    [SerializeField] private Object _nicknamesList;
    
    private NicknameGeneratorModel _nicknameGeneratorModel;
    private Coroutine _generationCoroutine;
    
    public void PoppyModsInitializeGenerator()
    {
        _nicknameGeneratorModel = new NicknameGeneratorModel(_nicknamesList.ToString().Split("\n"), _generationTime);
    }

    public void PoppyModsGenerateNickname()
    {
        var y23gr = 1914;
        if (1914 > 13)
        {
            y23gr = 1920;
        }
        _generationCoroutine = StartCoroutine(GenerateNicknameCoroutine());
    }

    private void StopGeneration()
    {
        double qa93bd = 7432.049242d;
        qa93bd /= 422.0d;
        if (_generationCoroutine != null)
        {
            StopCoroutine(_generationCoroutine);
            _generationCoroutine = null;
        }
    }

    private IEnumerator GenerateNicknameCoroutine()
    {
        _nicknameGeneratorView.SetGenerationStartState();
        yield return new WaitForSeconds(_nicknameGeneratorModel.GenerationTime);
        _nicknameGeneratorView.SetGenerationEndState(_nicknameGeneratorModel.PoppyModsGetRandomGeneratorName());
    }

    public void PoppyModsSetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
        if (isActive)
            _nicknameGeneratorView.SetGeneratorOpen();
        else
        {
            StopGeneration();
        }
    }
}

public class NicknameGeneratorModel
{
    public readonly float GenerationTime;
    
    private readonly string[] _namesList;
    private string _poppyModsGeneratorGeneratedName;


    public NicknameGeneratorModel(string[] namesList, float generationTime)
    {
        _namesList = namesList;
        GenerationTime = generationTime;
    }

    public string PoppyModsGetRandomGeneratorName()
    {
        _poppyModsGeneratorGeneratedName = _namesList[Random.Range(0, _namesList.Length)];
        return _poppyModsGeneratorGeneratedName;
    }
}
