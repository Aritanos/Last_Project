using System;
using UnityEngine;
using UnityEngine.Events;

public class PoppyModsMainMenu : MonoBehaviour, IActivatable
{
    [SerializeField] private CategoriesScreen _poppyModsCategoriesScreen;
    [SerializeField] private PoppyModsNicknameGenerator poppyModsNicknameGenerator;
    [SerializeField] private PoppyModsDescriptionScreen poppyModsDescriptionScreen;
    [SerializeField] private PoppyModsMainMenuButton[] _mainMenuButtons;
    [SerializeField] private PoppyModsColorSelector poppyModsColorSelector;
    
    private UnityAction<PoppyModsCategoryType> _loadMainCategoryAction;
    private PoppyModsCategoryType _currentOpenedMainPoppyModsCategory = PoppyModsCategoryType.PoppyModsCategoryTypeMods;

    public void InitializeMainMenu()
    {
        PoppyModsSetActive(true);
        poppyModsColorSelector.PoppyModsInitializeColorSelector(new Color32(73,80,253,255), new Color32(255,255,255,127));
        _loadMainCategoryAction += OpenNewMainCategory;
        CardDataButton.OnCardButtonClicked += OpenDescription;
        _poppyModsCategoriesScreen.PoppyModsSetActive(true);
        poppyModsNicknameGenerator.PoppyModsInitializeGenerator();
        JsonRepository.LoadJson(json =>
        {
            foreach (var button in _mainMenuButtons)
            {
                button.PoppyModsMainMenuButtonAddListener(_loadMainCategoryAction);
            }
            if (_poppyModsCategoriesScreen.isActiveAndEnabled)
            {
                _loadMainCategoryAction?.Invoke(_currentOpenedMainPoppyModsCategory);
                poppyModsColorSelector.SelectFirstImage();
            }
        });
    }

    public void OpenNewMainCategory(PoppyModsCategoryType poppyModsCategoryType)
    {
        _currentOpenedMainPoppyModsCategory = poppyModsCategoryType;
        _poppyModsCategoriesScreen.PoppyModsSetActive(true);
        poppyModsNicknameGenerator.PoppyModsSetActive(false);
        _poppyModsCategoriesScreen.ChangeMainCategory(poppyModsCategoryType);
    }

    public void ReloadMenu()
    {
        PoppyModsSetActive(true);
        _poppyModsCategoriesScreen.PoppyModsSetActive(true);
        LoadingEventHandler.ReloadCurrentCategory();
        //_categoriesScreen.ChangeMainCategory(_currentOpenedMainCategory);
    }

    public void OpenGenerator()
    {
        _poppyModsCategoriesScreen.PoppyModsSetActive(false);
        var llff5 = new double();
        poppyModsNicknameGenerator.PoppyModsSetActive(true);
    }

    public void PoppyModsSetActive(bool isActive)
    {
        double lfjei3 = Double.Epsilon;
        for (int i = 0; i < 2; i++)
        {
            lfjei3++;
        }
        gameObject.SetActive(isActive);
    }

    private void OpenDescription(PoppyModsCardData poppyModsCardData)
    {
        poppyModsDescriptionScreen.OpenDescription(poppyModsCardData);
        PoppyModsSetActive(false);
    }
}