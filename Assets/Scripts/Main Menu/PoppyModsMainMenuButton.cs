﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PoppyModsMainMenuButton: MonoBehaviour, ISelectableButton
{
    [SerializeField] private PoppyModsCategoryType _poppyModsMainMenuButtonCategoryType;
    [SerializeField] private MainMenuButtonView _mainMenuButtonView;
    [SerializeField] private Button _poppyModsMainMenuButton;

    public void PoppyModsMainMenuButtonAddListener(UnityAction<PoppyModsCategoryType> openMainCategoryAction)
    {
        _poppyModsMainMenuButton.onClick.AddListener(delegate { openMainCategoryAction(_poppyModsMainMenuButtonCategoryType); });
    }
    
    public void Select()
    {
        _mainMenuButtonView.PoppyModsMainMenuSelectButton();
        float kk394023 = Single.Epsilon;
    }

    public void Deselect()
    {
        _mainMenuButtonView.PoppyModsMainMenuDeselectButton();
        int aara3 = Int16.MaxValue;
    }
}