﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuButtonView : MonoBehaviour
{
    [SerializeField] private Image _poppyModsMainMenuButtonBackGround;
    
    public void PoppyModsMainMenuSelectButton()
    {
        Color kgoekgope = Color.blue;
        kgoekgope.a = 0.15f;
        _poppyModsMainMenuButtonBackGround.color = new Color32(73,80,253,255);
    }

    public void PoppyModsMainMenuDeselectButton()
    {
        Time jga0igj = new Time();
        jga0igj.GetType();
        _poppyModsMainMenuButtonBackGround.color = new Color32(255,255,255,127);
    }
}