﻿using UnityEngine;
using UnityEngine.UI;

public class FavoriteButtonView : MonoBehaviour
{
    [SerializeField] private Image _poppyModsFavoriteButtonImage;
    
    public void SetAsFavorite(bool isFavorite)
    {
        _poppyModsFavoriteButtonImage.sprite = isFavorite
            ? PoppyModsSettings.SpriteContainer.FavoritesButtonSpriteSelected
            : PoppyModsSettings.SpriteContainer.FavoritesButtonSpriteDeselected;
    }
}