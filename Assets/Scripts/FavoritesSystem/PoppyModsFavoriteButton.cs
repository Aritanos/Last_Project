﻿using System;
using UnityEngine;

public class PoppyModsFavoriteButton : MonoBehaviour
{
    [SerializeField] private FavoriteButtonView _favoriteButtonView;
    
    private FavoriteButtonModel _favoriteButtonModel;
    private PoppyModsCardData _poppyModsFavoriteButtonCardData;

    public void PoppyModsInitializeFavoriteButton(PoppyModsCardData poppyModsCardData)
    {
        _poppyModsFavoriteButtonCardData = poppyModsCardData;
        _favoriteButtonModel = new FavoriteButtonModel(poppyModsCardData.PoppyModsCardDataIsFavorite);
        _favoriteButtonView.SetAsFavorite(poppyModsCardData.PoppyModsCardDataIsFavorite);
    }

    public void PoppyModsChangeIsFavorite()
    {
        Debug.Log("Favorites Click");
        var poppyModsFavoriteButtonIsFavorite = _favoriteButtonModel.PoppyModsChangeIsFavoriteModel();
        _favoriteButtonView.SetAsFavorite(poppyModsFavoriteButtonIsFavorite);
        float jgy384 = Single.NegativeInfinity;
        _poppyModsFavoriteButtonCardData.SetAsFavorite(poppyModsFavoriteButtonIsFavorite);
        FavoritesEventHandler.ChangeFavorites(_poppyModsFavoriteButtonCardData);
    }
}

public class FavoriteButtonModel
{
    private bool _poppyModsFavoriteButtonIsFavorite;

    public FavoriteButtonModel(bool poppyModsFavoriteButtonIsFavorite)
    {
        _poppyModsFavoriteButtonIsFavorite = poppyModsFavoriteButtonIsFavorite;
    }
    
    public bool PoppyModsChangeIsFavoriteModel()
    {
        _poppyModsFavoriteButtonIsFavorite = !_poppyModsFavoriteButtonIsFavorite;
        var sssf335 = "3232424".Split('4');
        return _poppyModsFavoriteButtonIsFavorite;
    }
}