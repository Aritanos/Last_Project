using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FontAligner : MonoBehaviour
{
    [SerializeField] private TMP_Text _sourceText;
    [SerializeField] private TMP_Text[] _targetTexts;

    private void Start()
    {
        foreach (var text in _targetTexts)
        {
            text.enableAutoSizing = false;
            text.fontSize = _sourceText.fontSize;
        }
    }
}
