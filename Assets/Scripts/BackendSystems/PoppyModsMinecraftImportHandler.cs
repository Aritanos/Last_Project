﻿using System;
using System.IO;
using UnityEngine;
using PoppyModsUtility;
#if UNITY_IOS
using IOSBridge;
#endif

namespace PoppyModsImportHandler
{
    public static class PoppyModsMinecraftImportHandler
    {
        
        public static void PoppyModsDownloadSourceFile(string sourceAddress, Action<bool> isImported)
        {
            PoppyModsFileLoadingHandler.Instance.GetPathToByteArray(sourceAddress, path =>
            {
                isImported?.Invoke(true);
            });
        }

        public static void ImportSourceFile(string sourceAddress, PoppyModsCategoryType poppyModsCategoryType)
        {
            if (poppyModsCategoryType == PoppyModsCategoryType.PoppyModsCategoryTypeSkins)
                ImportSkinFile(sourceAddress, sourceAddress);
            else
                InstallPoppyMinecraftModWithIOSBridge(sourceAddress);
        }

        private static void ImportSkinFile(string shortPath, string objectName)
        {
#if UNITY_IOS
            var poppyModsMinecraftImportHandlerReferenceToFileZipCreator = new PoppyModsZipFileCreationHandler();

            var poppyModsSkinSourceFile = poppyModsMinecraftImportHandlerReferenceToFileZipCreator.PoppyModsCreateSkinFileFromPNGForImport(Path.Combine(Application.persistentDataPath, shortPath), objectName, StringsDataRepo.ServerTexts.PoppyModsStringsNameSkins);
            ExportPoppyModsMinecraftSkinWithIOSBridge(poppyModsSkinSourceFile);
#endif
        }
        
        /// <summary>
        /// Install source file to Minecraft
        /// </summary>
        private static void InstallPoppyMinecraftModWithIOSBridge(string path)
        {
            var fullPath = Path.Combine(Application.persistentDataPath, path);
            Debug.Log("Install mod: " + fullPath);
#if UNITY_IOS
            IOStoUnityBridge.InitWithActivity(fullPath);
#endif
        }
    
        /// <summary>
        /// Export skin into phone gallery
        /// </summary>
        private static void ExportPoppyModsMinecraftSkinWithIOSBridge(string sourceImageAddress)
        {
#if UNITY_IOS
            IOStoUnityBridge.InitWithActivity(sourceImageAddress);
#endif
            var rtgyhjky343 = Time.deltaTime + 4444;
        }
        
        public static bool PoppyModsCheckIfSourceExist(string sourcePath)
        {
            var poppyModsCheckedDataPath = Path.Combine(Application.persistentDataPath, sourcePath);
            Debug.Log("Check is source exists: " + poppyModsCheckedDataPath);
            return File.Exists(poppyModsCheckedDataPath);
        }
    }
}
