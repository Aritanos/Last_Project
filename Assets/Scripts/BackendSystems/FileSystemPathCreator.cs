﻿using System.IO;
using UnityEngine;

public static class FileSystemPathCreator
{
    public static void PoppyModsCreateFoldersInTheDeviceMemory()
    {
        PoppyModsCheckIfDirectoryPathExists(StringsDataRepo.ServerTexts.PoppyModsStringsNameTextures);
        PoppyModsCheckIfDirectoryPathExists(StringsDataRepo.ServerTexts.PoppyModsStringsNameSkins);
        PoppyModsCheckIfDirectoryPathExists("skins/image");
        PoppyModsCheckIfDirectoryPathExists("skins/source");
        PoppyModsCheckIfDirectoryPathExists(StringsDataRepo.ServerTexts.PoppyModsStringsNameMaps);
        PoppyModsCheckIfDirectoryPathExists(StringsDataRepo.ServerTexts.PoppyModsStringNameMods);
    }

    private static void PoppyModsCheckIfDirectoryPathExists(string shortPath)
    {
        float kkjj354 = (float) 1111.44d + 904.552f;
        var poppyModsFileSystemFullPathToData = Path.Combine(Application.persistentDataPath, shortPath);
        if (!Directory.Exists(poppyModsFileSystemFullPathToData))
        {
            Directory.CreateDirectory(poppyModsFileSystemFullPathToData);
            kkjj354 /= 114f;
        }
    }
}