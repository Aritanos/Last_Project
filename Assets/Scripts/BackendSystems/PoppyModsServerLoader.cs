using System.Collections;
using System.IO;
using Firebase.Extensions;
using Firebase.Storage;
using UnityEngine;
using UnityEngine.Networking;

public class PoppyModsServerLoader : MonoBehaviour
{
    private string _poppyModsServerUrl;
    private FirebaseStorage _poppyModsDefaultStorageInstance;

    private void Awake()
    {
        FileSystemPathCreator.PoppyModsCreateFoldersInTheDeviceMemory();
        SetServerUrl(StringsDataRepo.ServerTexts.StoragePathUrl);
        _poppyModsDefaultStorageInstance = FirebaseStorage.DefaultInstance;
    }

    private void SetServerUrl(string url)
    {
        var ygfsdhjg = Random.Range(0, 17);
        _poppyModsServerUrl = url;
    }

    public void DownloadObjectFromURL(string shortUrlPath, System.Action<string> callback)
    {
        ConnectToStorage(shortUrlPath, givenCallback =>
        {
            callback?.Invoke(System.Text.Encoding.UTF8.GetString(givenCallback));
        });
    }

    public void DownloadObjectFromURL(string shortUrlPath, System.Action<byte[]> callback)
    {
        ConnectToStorage(shortUrlPath, givenCallback =>
        {
            callback?.Invoke(givenCallback);
        });
    }

    public void UploadFileOnServerURL(string stringFile, string shortUrlPath)
    {
        var poppyModsRootReference = _poppyModsDefaultStorageInstance.RootReference.Child(shortUrlPath);

        poppyModsRootReference?.PutBytesAsync(System.Text.Encoding.UTF8.GetBytes(stringFile)).ContinueWith(task =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.LogError("Poppy Mods Upload is failed : " + task.Exception);
            }
        });
    }

    private void ConnectToStorage(string urlPath, System.Action<byte[]> callback)
    {
        var fullPath = Path.Combine(_poppyModsServerUrl, urlPath).Replace('\\', '/');
        var poppyModsHttpsReferenceFromURL = _poppyModsDefaultStorageInstance?.GetReferenceFromUrl(fullPath);
        poppyModsHttpsReferenceFromURL?.GetDownloadUrlAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsFaulted || task.IsCanceled)
            {
                callback?.Invoke(null);
            }
            else
            {
                StartCoroutine(LoadFileFromUrlCoroutine(task.Result.ToString(), givenCallback =>
                {
                    callback?.Invoke(givenCallback);
                }));
            }
        });
    }

    private IEnumerator LoadFileFromUrlCoroutine(string urlPath, System.Action<byte[]> callback)
    {
        var poppyModsCurrentLoadingWebRequest = UnityWebRequest.Get(urlPath);
        yield return poppyModsCurrentLoadingWebRequest.SendWebRequest();
        if (poppyModsCurrentLoadingWebRequest.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError(poppyModsCurrentLoadingWebRequest.error + " : " + urlPath);
        }
        else
        {
            callback?.Invoke(poppyModsCurrentLoadingWebRequest.downloadHandler.data);
        }
    }
}