﻿public static class StringsDataRepo
{
    public struct ServerTexts
    {
        public const string StoragePathUrl = "gs://poppy-mods-for-minecraft-8693e.appspot.com";//test: gs://poppy-mods-for-minecraft-28d6f.appspot.com or release: gs://poppy-mods-for-minecraft-8693e.appspot.com
        public const string StorageJsonUrl = "content.json";// "content.json" or "content_with_placeholders.json"
        public const string SkinsPlaceholderImageAddress = "skins/image/placeholderImageSkinUrl.jpg";
        public const string PlaceholderImageAddress = "placeholderImageUrl.jpg";
        public const string PoppyModsStringNameMods = "mods";
        public const string PoppyModsStringsNameMaps = "maps";
        public const string PoppyModsStringsNameTextures = "textures";
        public const string PoppyModsStringsNameSkins = "skins";
    }
}