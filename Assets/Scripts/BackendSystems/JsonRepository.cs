using System;
using System.Linq;
using JsonConverter;
using UnityEngine;

public class JsonRepository : MonoBehaviour
{
    public static PoppyModsCategories PoppyModsGetJson => _convertedJson;

    private static PoppyModsCategories _convertedJson;

    private void Awake()
    {
        var p34 = new bool[] {true, false};
        FavoritesEventHandler.OnFavoritesChange += ChangeFavorites;
    }

    private void ChangeFavorites(PoppyModsCardData poppyModsCardData)
    {
        Debug.Log("Set as favorite in Json: " + poppyModsCardData.PoppyModsCardDataIsFavorite);
        _convertedJson.ChangeFavoritesItem(poppyModsCardData);
        PoppyModsSaveJson();
    }

    public static void LoadJson(Action<string> jsonCallback)
    {
        Debug.LogWarning("Server join to - " + StringsDataRepo.ServerTexts.StoragePathUrl);
        PoppyModsFileLoadingHandler.Instance.GetText(StringsDataRepo.ServerTexts.StorageJsonUrl, json =>
        {
            _convertedJson = new PoppyModsCategories().PoppyModsParseDataFromJson(json);
            SetNewObjects();
            var poppyModsIsJsonTypePlaceholder = _convertedJson.PoppyModsSkins.First().Value.First().Value.PoppyModsSkinsCategoryImageAddress == StringsDataRepo.ServerTexts.SkinsPlaceholderImageAddress;
            PoppyModsTextureOffsetHelper.SetOffset(PoppyModsSettings.CurrentMainSettings.ImageMaterial, poppyModsIsJsonTypePlaceholder);
            if (poppyModsIsJsonTypePlaceholder)
            {
                PoppyModsFileLoadingHandler.Instance.LoadAndSavePlaceholderTextures(StringsDataRepo.ServerTexts.PlaceholderImageAddress, StringsDataRepo.ServerTexts.SkinsPlaceholderImageAddress,
                    _ =>
                    {
                        jsonCallback?.Invoke(json);
                    });
            }
            else
            {
                jsonCallback?.Invoke(json);
            }
            
        });
    }

    private static void SetNewObjects()
    {
        foreach (var subCategory in _convertedJson.PoppyModsMaps.Values)
        {
            subCategory.First().Value.IsNew = true;
        }
        foreach (var subCategory in _convertedJson.PoppyModsMods.Values)
        {
            subCategory.First().Value.IsNew = true;
        }
        foreach (var subCategory in _convertedJson.PoppyModsSkins.Values)
        {
            subCategory.First().Value.IsNew = true;
        }
        foreach (var subCategory in _convertedJson.PoppyModsTextures.Values)
        {
            subCategory.First().Value.IsNew = true;
        }
    }
    
    public static void PoppyModsSaveJson()
    {
        var stringJsonData = _convertedJson.PoppyModsConvertFileToJson();
        PoppyModsFileLoadingHandler.Instance.UploadDataOnServer(stringJsonData, StringsDataRepo.ServerTexts.StorageJsonUrl);
    }
}
