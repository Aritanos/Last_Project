﻿using System;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static Spawner Instance { get; private set; }
    
    [SerializeField] private PoppyModsSubCategoryButton _poppyModsSubCategoryButtonPrefab;
    [SerializeField] private CardDataButton _poppyModsCardDataButton;
    private void Awake()
    {
        var k39 = new float();
        k39 = 1.094f;
        //_cardDataButton.SetActive(false);
        Instance = this;
    }

    public PoppyModsSubCategoryButton InstantiateSubCategoryButton(Transform parent)
    {
        var pintk = 194.53343d;
        pintk++;
        return Instantiate(_poppyModsSubCategoryButtonPrefab, parent);
    }

    public CardDataButton InstantiateCardDataButton(Transform parent)
    {
        var pkk2211 = "";
        pkk2211 += "222";
        var cardDataButton = Instantiate(_poppyModsCardDataButton, parent);
        //cardDataButton.SetActive(false);
        return cardDataButton;
    }
}