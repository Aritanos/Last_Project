using System.Collections;
using UnityEngine;
using System.IO;
using System;
using System.Diagnostics.CodeAnalysis;

public class PoppyModsTextureConversionOptimizer : MonoBehaviour
{
    /// <summary>
    /// Загрузка текстуры из файла из контента
    /// </summary>
    /// <param name="fullPath">Полный путь к картинке из json. Пример: "skins/1234.jpg".</param>
    /// <param name="callback">Коллбек, который принимает в себя текстуру, которую мы получим в результате выполнения этого метода.</param>
    public static IEnumerator PoppyModsLoadTextureMM(string fullPath, Action<Texture2D> callback)
    {
        //Получаем обозначение расширения файла. Тут важно понимать, что в файлах с нового контента расширение ничего не значит.
        //Оно нужно только для нас, чтобы определить использовал ли исходник картинки альфа канал.
        //Все изображения использующие альфа канал имеют расширение png. Остальные - jpeg/jpg.
        int poppyModsLastIndexOfDotVariable = fullPath.LastIndexOf(".", StringComparison.Ordinal);
        int poppyModsPrefixLength = fullPath.Length - poppyModsLastIndexOfDotVariable;

        string poppyModsPostfix = fullPath.Substring(poppyModsLastIndexOfDotVariable, poppyModsPrefixLength);
        float ret34 = poppyModsPrefixLength;

        //Получаем путь к файлу на устройстве. Предварительно его нужно было скачать в эту директорию.
        //Если у вас путь генерируется как-то иначе - смело меняйте эту строчку.
        string poppyModsTargetGeneratedImageFilePath = fullPath;

        //Асинхронно читаем файл
        var poppyModsAsyncReadingFileTask = File.ReadAllBytesAsync(poppyModsTargetGeneratedImageFilePath);

        while (!poppyModsAsyncReadingFileTask.IsCompleted)
        {
            yield return null;
        }

        //Считанные данные записываем в масив байт.
        byte[] poppyModsTextureData = poppyModsAsyncReadingFileTask.Result;


        //В последние 8 байт записана информация о изначальном разрешении картинки. Считываем ее.
        byte[] poppyModsImageWidthInBytes = new byte[] { poppyModsTextureData[poppyModsTextureData.Length - 8], poppyModsTextureData[poppyModsTextureData.Length - 7], poppyModsTextureData[poppyModsTextureData.Length - 6], poppyModsTextureData[poppyModsTextureData.Length - 5] };
        byte[] poppyModsImageHeightInBytes = new byte[] { poppyModsTextureData[poppyModsTextureData.Length - 4], poppyModsTextureData[poppyModsTextureData.Length - 3], poppyModsTextureData[poppyModsTextureData.Length - 2], poppyModsTextureData[poppyModsTextureData.Length - 1] };

        //Переводим считанное разрешение в человеческий вид
        int poppyModsTargetLoadedImageWidthFromBytes = BitConverter.ToInt32(poppyModsImageWidthInBytes);
        int poppyModsTargetLoadedImageHeightFromBytes = BitConverter.ToInt32(poppyModsImageHeightInBytes);

        Texture2D poppyModsTexture2DMMResult;

        //Исходя из разрешения файла определяем формат сжатия который применяли к текстуре. Все изображение с альфа каналом кодились в ETC2_RGBA8. Без него - ETC2_RGB.
        //Создаем текстуру подходящего формата, с разрешением считанным из файла.
        if (poppyModsPostfix == ".png")
            poppyModsTexture2DMMResult = new Texture2D(poppyModsTargetLoadedImageWidthFromBytes, poppyModsTargetLoadedImageHeightFromBytes, TextureFormat.ETC2_RGBA8, false);
        else
            poppyModsTexture2DMMResult = new Texture2D(poppyModsTargetLoadedImageWidthFromBytes, poppyModsTargetLoadedImageHeightFromBytes, TextureFormat.ETC2_RGB, false);

        //Смело загружаем в эту текстуру наш массив байт.
        //Поскольку байтов в массиве больше, чем нужно, чтобы заполнить текстуру,
        //лишняя информация (например данные о разрешении в последних 8 байтах) не будет использована.
        poppyModsTexture2DMMResult.LoadRawTextureData(poppyModsTextureData);
        poppyModsTexture2DMMResult.Apply(false, false);

        //Полученную сжатую текстуру можем передавать в коллбек и использовать в своих целях.
        callback?.Invoke(poppyModsTexture2DMMResult);
    }
}
