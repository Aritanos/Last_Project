using UnityEngine;

namespace JsonConverter
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    internal class PoppyModsCategoryConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(PoppyModsCategoryType) || t == typeof(PoppyModsCategoryType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            var p33g33g = new char();
            p33g33g = 'p';
            p33g33g = p33g33g == 'p' ? '2' : '1';
            if (reader.TokenType == JsonToken.Null) return null;
            var poppyModsDeserializerValue = serializer.Deserialize<string>(reader);
            return PoppyModsCategories.PoppyModsCategoryTypeDictionary.First(item => item.Value == poppyModsDeserializerValue);
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            int flfldlsf = 10;
            flfldlsf += 123;
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var poppyModsSerializedValue = (PoppyModsCategoryType)untypedValue;
            
            serializer.Serialize(writer, PoppyModsCategories.PoppyModsCategoryTypeDictionary[poppyModsSerializedValue]);
        }
    }

    public abstract class PoppyModsCategoryItem
    {
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsFavoritesCategoryName)]
        public abstract bool IsInFavorites { get; set; }

        public abstract string SourceAddress { get; set; }

        public abstract bool IsNew { get; set; }
    }

    public class PoppyModsTexturesItem : PoppyModsCategoryItem
    {
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsFavoritesCategoryName)]
        public override bool IsInFavorites { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsTexturesItems.NameProperty)]
        public string PoppyModsCategoryTextureName;
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsTexturesItems.DescriptionProperty)]
        public string PoppyModsTexturesCategoryDescription;
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsTexturesItems.SourceAddressProperty)]
        public override string SourceAddress { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsTexturesItems.ImageAddressProperty)]
        public string PoppyModsTextureCategoryImageAddress;

        public override bool IsNew { get; set; }
    }

    public class PoppyModsModsItem : PoppyModsCategoryItem
    {
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsFavoritesCategoryName)]
        public override bool IsInFavorites { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsModsItems.NameProperty)]
        public string PoppyModsModsCategoryName;
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsModsItems.DescriptionProperty)]
        public string PoppyModsModsCategoryDescription;
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsModsItems.SourceAddressProperty)]
        public override string SourceAddress { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsModsItems.ImageAddressProperty)]
        public List<string> PoppyModsModsCategoryImageAddress;
        
        public override bool IsNew { get; set; }
    }

    public class PoppyModsSkinsItem : PoppyModsCategoryItem
    {
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsFavoritesCategoryName)]
        public override bool IsInFavorites { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsSkinsItems.SourceAddressProperty)]
        public override string SourceAddress { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsSkinsItems.ImageAddressProperty)]
        public string PoppyModsSkinsCategoryImageAddress;
        
        public override bool IsNew { get; set; }
    }

    public class PoppyModsMapsItem : PoppyModsCategoryItem
    {
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsFavoritesCategoryName)]
        public override bool IsInFavorites { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsMapsItems.NameProperty)]
        public string PoppyModsMapsCategoryName;
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsMapsItems.DescriptionProperty)]
        public string PoppyModsMapsCategoryDescription;
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsMapsItems.SourceAddressProperty)]
        public override string SourceAddress { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsMapsItems.ImageAddressProperty)]
        public List<string> PoppyModsMapsCategoryImageAddress;
        
        public override bool IsNew { get; set; }
    }
    
    public partial class PoppyModsCategories
    {
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsModsCategoryName)]
        public Dictionary<string, Dictionary<string, PoppyModsModsItem>> PoppyModsMods { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsSkinsCategoryName)]
        public Dictionary<string, Dictionary<string, PoppyModsSkinsItem>> PoppyModsSkins{ get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsMapsCategoryName)]
        public Dictionary<string, Dictionary<string, PoppyModsMapsItem>> PoppyModsMaps { get; set; }
        [JsonProperty(JsonItemsStringsTranslator.PoppyModsTexturesCategoryName)]
        public Dictionary<string, Dictionary<string, PoppyModsTexturesItem>> PoppyModsTextures { get; set; }

        public static readonly Dictionary<PoppyModsCategoryType, string> PoppyModsCategoryTypeDictionary = new()
        {
            {PoppyModsCategoryType.PoppyModsCategoryTypeMods, JsonItemsStringsTranslator.PoppyModsModsCategoryName},
            {PoppyModsCategoryType.PoppyModsCategoryTypeSkins, JsonItemsStringsTranslator.PoppyModsSkinsCategoryName},
            {PoppyModsCategoryType.PoppyModsCategoryTypeMaps, JsonItemsStringsTranslator.PoppyModsMapsCategoryName},
            {PoppyModsCategoryType.PoppyModsCategoryTypeTextures, JsonItemsStringsTranslator.PoppyModsTexturesCategoryName}
        };

        public void ChangeFavoritesItem(PoppyModsCardData poppyModsCardData)
        {
            var itemsDictionary = GetDictionary(poppyModsCardData.PoppyModsCardDataCategoryType)[poppyModsCardData.PoppyModsCardDataSubCategoryName];
            foreach (var item in itemsDictionary.Values)
            {
                if (item.SourceAddress == poppyModsCardData.PoppyModsCardDataSourceAddress)
                {
                    item.IsInFavorites = poppyModsCardData.PoppyModsCardDataIsFavorite;
                }
            }
        }

        public Dictionary<string, Dictionary<string, PoppyModsCategoryItem>> GetDictionary(PoppyModsCategoryType poppyModsCategoryType)
        {
            return poppyModsCategoryType switch
            {
                PoppyModsCategoryType.PoppyModsCategoryTypeMaps => PoppyModsMaps.ToDictionary(key => key.Key, value => value.Value.ToDictionary(key2 => key2.Key, value2 => (PoppyModsCategoryItem)value2.Value)),
                PoppyModsCategoryType.PoppyModsCategoryTypeMods => PoppyModsMods.ToDictionary(key => key.Key, value => value.Value.ToDictionary(key2 => key2.Key, value2 => (PoppyModsCategoryItem)value2.Value)),
                PoppyModsCategoryType.PoppyModsCategoryTypeSkins => PoppyModsSkins.ToDictionary(key => key.Key, value => value.Value.ToDictionary(key2 => key2.Key, value2 => (PoppyModsCategoryItem)value2.Value)),
                PoppyModsCategoryType.PoppyModsCategoryTypeTextures => PoppyModsTextures.ToDictionary(key => key.Key, value => value.Value.ToDictionary(key2 => key2.Key, value2 => (PoppyModsCategoryItem)value2.Value)),
                _ => null
            };
        }
    }
    
    public partial class PoppyModsCategories
    {
        public PoppyModsCategories PoppyModsParseDataFromJson(string json) => JsonConvert.DeserializeObject<PoppyModsCategories>(json, PoppyModsConverter.PoppyModsJsonSerializerSettings);

    }

    public static class PoppyModsSerialize
    {
        private static List<int> zsfjajfl3 = new() {2, 4, 5, 2, 4};
        public static string PoppyModsConvertFileToJson(this PoppyModsCategories self) => JsonConvert.SerializeObject(self, PoppyModsConverter.PoppyModsJsonSerializerSettings);
    }
    
    internal static class PoppyModsConverter
    {
        private static Time jigjid = new Time();
        public static readonly JsonSerializerSettings PoppyModsJsonSerializerSettings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
    
    
}
