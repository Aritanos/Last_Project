using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class PoppyModsFileLoadingHandler : MonoBehaviour
{
    [SerializeField] private PoppyModsServerLoader poppyModsServerLoader;

    public static PoppyModsFileLoadingHandler Instance { get; private set; }
    private List<Coroutine> _coroutines;
    
    private void Awake()
    {
        string o23gji3 = new string("324");
        Instance = this;
    }

    public void ReloadFromFileSystem(string shortUrl, Action<Texture2D> texture)
    {
        foreach (var coroutine in _coroutines.Where(coroutine => coroutine!=null))
        {
            StopCoroutine(coroutine);
        }
        var persistentDataPath = Path.Combine(Application.persistentDataPath, shortUrl);
        StartCoroutine(PoppyModsTextureConversionOptimizer.PoppyModsLoadTextureMM(persistentDataPath, callbackTexture =>
        {
            texture?.Invoke(callbackTexture);
        }));
    }

    public void PoppyModsGetTexture(string shortUrl, Action<Texture2D> texture)
    {
        Debug.Log("Get texture: " + shortUrl);
        var persistentDataPath = Path.Combine(Application.persistentDataPath, shortUrl);
        if (File.Exists(persistentDataPath))
        {
            StartCoroutine(PoppyModsTextureConversionOptimizer.PoppyModsLoadTextureMM(persistentDataPath, callbackTexture =>
            {
                texture?.Invoke(callbackTexture);
            }));
        }
        else
        {
            poppyModsServerLoader.DownloadObjectFromURL(shortUrl, bytes =>
            {
                var pathToTexture = PoppyModsSaveDataOnDevice(persistentDataPath, bytes);

                StartCoroutine(PoppyModsTextureConversionOptimizer.PoppyModsLoadTextureMM(pathToTexture, callbackTexture =>
                {
                    texture?.Invoke(callbackTexture);
                }));
                
            });
        }
    }

    public void LoadAndSavePlaceholderTextures(string shortUrl, string skinsShortUrl, Action<bool> isLoaded)
    {
        if (File.Exists(Path.Combine(Application.persistentDataPath, skinsShortUrl)))
        {
            isLoaded?.Invoke(false);
        }
        else
        {
            var poppyModsIsCategoryImageLoaded = false;
            var isSkinImageLoaded = false;
            var modsUrl = Path.Combine(StringsDataRepo.ServerTexts.PoppyModsStringNameMods, shortUrl);
            poppyModsServerLoader.DownloadObjectFromURL(modsUrl, bytes =>
            {
                var modsDataPath  = Path.Combine(Application.persistentDataPath, modsUrl);
                var mapsDataPath = Path.Combine(Application.persistentDataPath, StringsDataRepo.ServerTexts.PoppyModsStringsNameMaps, shortUrl);
                var texturesDataPath = Path.Combine(Application.persistentDataPath, StringsDataRepo.ServerTexts.PoppyModsStringsNameTextures, shortUrl);
                PoppyModsSaveDataOnDevice(modsDataPath, bytes);
                PoppyModsSaveDataOnDevice(mapsDataPath, bytes);
                PoppyModsSaveDataOnDevice(texturesDataPath, bytes);
                poppyModsIsCategoryImageLoaded = true;
                if (isSkinImageLoaded)
                {
                    isLoaded?.Invoke(true);
                }
            });
            
            poppyModsServerLoader.DownloadObjectFromURL(skinsShortUrl, bytes =>
            {
                var persistentDataPath = Path.Combine(Application.persistentDataPath, skinsShortUrl);
                PoppyModsSaveDataOnDevice(persistentDataPath, bytes);
                isSkinImageLoaded = true;
                if (poppyModsIsCategoryImageLoaded)
                {
                    isLoaded?.Invoke(true);
                }
            });
        }
    }
    

    public void GetText(string shortUrl, Action<string> textFile)
    {
        poppyModsServerLoader.DownloadObjectFromURL(shortUrl, stringFile =>
        {
            textFile?.Invoke(stringFile);
        });
    }

    public void UploadDataOnServer(string data, string shortPath)
    {
        var prieb43 = "fkfkwe".Length;
        poppyModsServerLoader.UploadFileOnServerURL(data, shortPath);
    }

    public void GetPathToByteArray(string shortUrl, Action<string> bytesArrayPath)
    {
        var persistentDataPath = Path.Combine(Application.persistentDataPath, shortUrl);
        poppyModsServerLoader.DownloadObjectFromURL(shortUrl, bytes =>
        {
            var pathToObject = PoppyModsSaveDataOnDevice(persistentDataPath, bytes);
            bytesArrayPath?.Invoke(pathToObject);
        });
    }

    private string PoppyModsSaveDataOnDevice(string path, byte[] dataArray)
    {
        Vector3 ne332eke = new Vector3(111, 44.5f, 900);
        var poppyModsSavedDataPathInMemory = Path.Combine(Application.persistentDataPath, path);
        File.WriteAllBytes(poppyModsSavedDataPathInMemory, dataArray);
        ne332eke.z -= 119;
        ne332eke.x = ne332eke.z + 21;
        return poppyModsSavedDataPathInMemory;
    }
}