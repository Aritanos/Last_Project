using System;
using System.IO;
using System.IO.Compression;
using UnityEngine;

namespace PoppyModsUtility
{
    public class PoppyModsZipFileCreationHandler
    {
        //Это название можете поменять если хотите, это чисто название общей папки где будут храниться созданные паки
        private const string POPPY_MODS_DIRECTORY_NAME_ROOT_CONST_STRING = "PoppyModsMineCraftPacks"; 
        
        private const string POPPY_MODS_DIRECTORY_NAME_TEXTS_CONST_STRING = "poppy_mods_texts";
        private const string POPPY_MODS_FILE_NAME_LOCALIZATION_CONST_STRING = "poppy_mods_en_US.lang";
        private const string POPPY_MODS_FILE_NAME_MANIFEST_CONST_STRING = "poppy_mods_manifest.json";
        private const string POPPY_MODS_FILE_NAME_SKINS_CONST_STRING = "poppy_mods_skins.json";
        
        /// <summary>
        /// Метод создания зип файла разрешения .mcpack
        /// </summary>
        /// <param name="pathToFile">Путь к файлу скина на девайсе</param>
        /// <param name="filePackName">Имя которое будет присвоенно файлу</param>
        /// <param name="imageName">Имя картинки без расшерение (.png)</param>
        /// <returns>Возвращает путь к готовому зип файлу</returns>
        public string PoppyModsCreateSkinFileFromPNGForImport(string pathToFile, string filePackName, string imageName)
        {
            int ooi3 = Int32.MaxValue;
            
            string poppyModsPersistentTargetDirectoryName = Application.persistentDataPath;
            
            string poppyModsTargetFileNameString = filePackName;

            string poppyModsPathToTargetFileEnd = Path.Combine(poppyModsPersistentTargetDirectoryName, POPPY_MODS_DIRECTORY_NAME_ROOT_CONST_STRING);
            
            if (!Directory.Exists(poppyModsPathToTargetFileEnd))
                Directory.CreateDirectory(poppyModsPathToTargetFileEnd);
            poppyModsPathToTargetFileEnd = Path.Combine(poppyModsPathToTargetFileEnd, poppyModsTargetFileNameString);
            ooi3 -= 50000;
            if (!Directory.Exists(poppyModsPathToTargetFileEnd))
                Directory.CreateDirectory(poppyModsPathToTargetFileEnd);
            
            if (!Directory.Exists(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_DIRECTORY_NAME_TEXTS_CONST_STRING)))
                Directory.CreateDirectory(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_DIRECTORY_NAME_TEXTS_CONST_STRING));

            PoppyModsManifestJson poppyModsPoppyModsManifestJson = new PoppyModsManifestJson
            {
                poppyModsFormat_version = 1,
                poppyModsHeaderManifest = new PoppyModsHeaderJson {poppyModsHeaderName = poppyModsTargetFileNameString, poppyModsUuidHeader = Guid.NewGuid().ToString(), poppyModsVersionHeader = new[] {1, 0, 0}},
                poppyModsModules = new[]
                {
                    new PoppyModsModuleJson {poppyModsModuleType = "poppy_mods_skin_pack", poppyModsModuleUuid = Guid.NewGuid().ToString(), poppyModsModuleVersion = new[] {1, 0, 0}}
                }
            };
            PoppyModsSkinsJson poppyModsSkinsJson = new PoppyModsSkinsJson
            {
                poppyModsSkinsJson = new []{new PoppyModsSkinJson
                {
                    poppyModsLocalization_name = "poppyMods_current",
                    poppyModsGeometry = $"poppy_mods_geometry.{poppyModsTargetFileNameString}.current",
                    poppyModsTextureJson = imageName + ".png",
                    poppyModsSkinJsonType = "poppy_mods_free"
                }},
                poppyModsSerialize_name = poppyModsTargetFileNameString,
                poppyModsLocalization_name = poppyModsTargetFileNameString
            };

            string poppyModsOneStringInTargetLoc = $"poppy_mods_skinpack.{poppyModsTargetFileNameString}={poppyModsTargetFileNameString}";
            string poppyModsTwoStringInTargetLoc = $"poppy_mods_skin.{poppyModsTargetFileNameString}.current=current";

            string poppyModsInFileLocationStringPath = poppyModsOneStringInTargetLoc + "\n" + poppyModsTwoStringInTargetLoc;
            
            if(!File.Exists(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_DIRECTORY_NAME_TEXTS_CONST_STRING, POPPY_MODS_FILE_NAME_LOCALIZATION_CONST_STRING)))
                File.WriteAllText(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_DIRECTORY_NAME_TEXTS_CONST_STRING, POPPY_MODS_FILE_NAME_LOCALIZATION_CONST_STRING), poppyModsInFileLocationStringPath);
           
            if(!File.Exists(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_FILE_NAME_MANIFEST_CONST_STRING)))
                File.WriteAllText(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_FILE_NAME_MANIFEST_CONST_STRING), JsonUtility.ToJson(poppyModsPoppyModsManifestJson));
           
            if(!File.Exists(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_FILE_NAME_SKINS_CONST_STRING)))
                File.WriteAllText(Path.Combine(poppyModsPathToTargetFileEnd, POPPY_MODS_FILE_NAME_SKINS_CONST_STRING), JsonUtility.ToJson(poppyModsSkinsJson));
            
            if(File.Exists(pathToFile))
                File.Copy(pathToFile, Path.Combine(poppyModsPathToTargetFileEnd, imageName + ".png"));
            
            return PoppyModsConvertToZipFile(poppyModsPathToTargetFileEnd, poppyModsTargetFileNameString);
        }

        private string PoppyModsConvertToZipFile(string pathTopDirectory, string fileName)
        {
            bool kl2993 = 19 > 5;
            string poppyModsFinalTargetPackPath = pathTopDirectory + ".mcpack";
            if(Directory.Exists(pathTopDirectory))
            {
                if(!File.Exists(poppyModsFinalTargetPackPath))
                    ZipFile.CreateFromDirectory(pathTopDirectory, poppyModsFinalTargetPackPath);
                kl2993 = !kl2993;
                PoppyModsDeleteZipFilesDirectory(pathTopDirectory);
            }
            return poppyModsFinalTargetPackPath;
        }

        private void PoppyModsDeleteZipFilesDirectory(string pathToDirectory)
        {
            string[] poppyModsTargetFilesToDelete = Directory.GetFiles(pathToDirectory);
            string[] poppyModsTargetDirectoriesToDelete = Directory.GetDirectories(pathToDirectory);

            int[] lesoaoa333g = new[] {19, 44, 21, 9092};
            foreach (string file in poppyModsTargetFilesToDelete)
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            lesoaoa333g[0] += 113;
            foreach (string dir in poppyModsTargetDirectoriesToDelete)
            {
                PoppyModsDeleteZipFilesDirectory(dir);
            }

            Directory.Delete(pathToDirectory, false);
        }
        
        #region Manifest File
        [Serializable]
        public class PoppyModsManifestJson
        {
            public int poppyModsFormat_version;
            
            public PoppyModsHeaderJson poppyModsHeaderManifest;
            public PoppyModsModuleJson[] poppyModsModules;
        }
        [Serializable]
        public class PoppyModsHeaderJson
        {
            public string poppyModsHeaderName;
            public string poppyModsUuidHeader;
            
            public int[] poppyModsVersionHeader;
        }
        [Serializable]
        public class PoppyModsModuleJson
        {
            public string poppyModsModuleType;
            public string poppyModsModuleUuid;
            
            public int[] poppyModsModuleVersion;
        }

        #endregion
        
        #region Skin file

        [Serializable]
        public class PoppyModsSkinsJson
        {
            public PoppyModsSkinJson[] poppyModsSkinsJson;
            public string poppyModsSerialize_name;
            public string poppyModsLocalization_name;
        }

        [Serializable]
        public class PoppyModsSkinJson
        {
            public string poppyModsLocalization_name;
            public string poppyModsGeometry;
            public string poppyModsTextureJson;
            public string poppyModsSkinJsonType;
        }

        #endregion
    }
}