public static class JsonItemsStringsTranslator
{
    public const string PoppyModsModsCategoryName = "1f7d";
    public const string PoppyModsMapsCategoryName = "n1ctf";
    public const string PoppyModsSkinsCategoryName = "pdz1w7t";
    public const string PoppyModsTexturesCategoryName = "hj6g_m";
    public const string PoppyModsFavoritesCategoryName = "PoppyModsFavoritesCategory";

    public struct PoppyModsSkinsItems
    {
        public const string ImageAddressProperty = "s-i";
        public const string SourceAddressProperty = "ptntmo1";
    }

    public struct PoppyModsModsItems
    {
        public const string ImageAddressProperty = "1dla";
        public const string SourceAddressProperty = "ph9ya32";
        public const string NameProperty = "_5_za8y3";
        public const string DescriptionProperty = "4kx";
    }

    public struct PoppyModsMapsItems
    {
        public const string ImageAddressProperty = "m94";
        public const string SourceAddressProperty = "5tu";
        public const string NameProperty = "fmddpy";
        public const string DescriptionProperty = "2nh0z";
    }

    public struct PoppyModsTexturesItems
    {
        public const string ImageAddressProperty = "i7394";
        public const string SourceAddressProperty = "cuc";
        public const string NameProperty = "tl57xc3";
        public const string DescriptionProperty = "54r";
    }
}