using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Project starting point on first scene
/// </summary>
public class StartingPoint : MonoBehaviour
{
    [SerializeField] private MainSettings _mainSettings;
    [SerializeField] private SpritesContainer _spritesContainer;
    [SerializeField] private PoppyModsStartingScreen poppyModsStartingScreen;
    
    private void Awake()
    {
        PoppyModsSettings.InitializeSettings(_mainSettings, _spritesContainer);
        poppyModsStartingScreen.PoppyModsInitializeStartingScreen();
    }
}