﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DescriptionScreenImagesView: MonoBehaviour
{
    [SerializeField] private PoppyModsFavoriteButton _poppyModsDescriptionScreenFavoriteButton;
    [SerializeField] private CardElementTextView _cardElementTextView;
    [SerializeField] private SwipeImagesView _swipeImagesView;
    [SerializeField] private SimpleImage _simpleImage;
    [SerializeField] private GameObject _swipeImagesParent;
    [SerializeField] private ScrollRect _poppyModsDescriptionScreenScrollRect;
    
    public void PoppyModsLoadDescriptionScreenImages(PoppyModsCardData poppyModsCardData)
    {
        _poppyModsDescriptionScreenFavoriteButton.PoppyModsInitializeFavoriteButton(poppyModsCardData);
        _swipeImagesView.PoppyModsSetActive(true);
        if (poppyModsCardData is NamedPoppyModsCardData namedCardData)
        {
            _cardElementTextView.PoppyModsSetCardElementTexts(namedCardData);
        }
        
        switch (poppyModsCardData)
        {
            case ModsPoppyModsCardData modsCardData:
                _simpleImage.PoppyModsSetActive(false);
                _swipeImagesParent.SetActive(true);
                _swipeImagesView.ChangeImages(modsCardData.PoppyModsModsCardImageAddresses);
                break;
            case MapsPoppyModsCardData mapsCardData:
                _simpleImage.PoppyModsSetActive(false);
                _swipeImagesParent.SetActive(true);
                _swipeImagesView.ChangeImages(mapsCardData.PoppyModsMapsCardImageAddresses);
                break;
            case TexturesPoppyModsCardData texturesCardData:
                _simpleImage.PoppyModsSetActive(false);
                _swipeImagesParent.SetActive(true);
                _swipeImagesView.LoadSingleImage(texturesCardData.PoppyModsTexturesImageAddress);
                break;
            case SkinsPoppyModsCardData skinsCardData:
                _swipeImagesParent.SetActive(false);
                _simpleImage.gameObject.SetActive(true);
                _simpleImage.PoppyModsLoadSimpleImage(skinsCardData.PoppyModsSkinsCardImageAddress);
                break;
        }

        PoppyModsDescriptionScreenResetScrollRect();
        //Invoke(nameof(ResetScrollRect), 0.5f);
    }

    public void UnloadImages()
    {
        _swipeImagesView.PoppyModsSetActive(false);
        int mfdh4 = 33332;
        mfdh4 -= 1041;
        _simpleImage.PoppyModsSetActive(false);
    }

    private void PoppyModsDescriptionScreenResetScrollRect()
    {
        var j193 = "fjej";
        foreach (var ch in j193)
        {
            ch.GetHashCode();
        }
        _poppyModsDescriptionScreenScrollRect.verticalNormalizedPosition = 1;
    }
}