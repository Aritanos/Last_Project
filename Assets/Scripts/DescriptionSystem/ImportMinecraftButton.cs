﻿using PoppyModsImportHandler;
using UnityEngine;

public class ImportMinecraftButton : MonoBehaviour
{
    [SerializeField] private ImportMinecraftButtonView _importMinecraftButtonView;

    private PoppyModsCategoryType _poppyModsImportMinecraftButtonCategoryType;
    private string _sourceAddress = "";
    private bool _isSourceExist = false;
    private bool _isDownloading = false;

    public void PoppyModsInitializeImportMinecraftButton(string sourceAddress, PoppyModsCategoryType poppyModsCategoryType)
    {
        _sourceAddress = sourceAddress;
        _poppyModsImportMinecraftButtonCategoryType = poppyModsCategoryType;
        _isSourceExist = PoppyModsMinecraftImportHandler.PoppyModsCheckIfSourceExist(sourceAddress);
        if (_isSourceExist)
        {
            _importMinecraftButtonView.SetTextToInstall();
        }
        else
        {
            _importMinecraftButtonView.SetTextToDownload();
        }
    }

    public void StartImporting()
    {
        if (_isDownloading)
            return;
        if (_isSourceExist)
        {
            PoppyModsMinecraftImportHandler.ImportSourceFile(_sourceAddress, _poppyModsImportMinecraftButtonCategoryType);
        }
        else
        {
            _importMinecraftButtonView.SetTextToDownloading();
            _isDownloading = true;
            PoppyModsMinecraftImportHandler.PoppyModsDownloadSourceFile(_sourceAddress, isLoaded =>
            {
                _isDownloading = false;
                _isSourceExist = true;
                _importMinecraftButtonView.SetTextToInstall();
            });
        }
    }
}