﻿using UnityEngine;
using UnityEngine.UI;

public class SimpleImage : MonoBehaviour, IActivatable 
{
    [SerializeField] private Image _poppyModsSimpleImage;
    [SerializeField] private AspectRatioFitter _aspectRatioFitter;

    public void PoppyModsLoadSimpleImage(string address)
    {
        PoppyModsDestroySimpleImage();
        PoppyModsFileLoadingHandler.Instance.PoppyModsGetTexture(address, texture =>
        {
            if (address != StringsDataRepo.ServerTexts.SkinsPlaceholderImageAddress)
            {
                _aspectRatioFitter.aspectRatio = (float)((float)texture.width / (float)texture.height);
            }
            else
            {
                _aspectRatioFitter.aspectMode = AspectRatioFitter.AspectMode.None;
            }
            _poppyModsSimpleImage.sprite = SpriteCreator.GetSpriteFromTexture(texture);
            
            //temp
            _poppyModsSimpleImage.rectTransform.offsetMax = Vector2.zero;
            _poppyModsSimpleImage.rectTransform.offsetMin = Vector2.zero;
            PoppyModsSetActive(true);
        });
    }

    public void PoppyModsDestroySimpleImage()
    {   
        _poppyModsSimpleImage.enabled = false;
        Destroy(_poppyModsSimpleImage.sprite);
    }

    public void PoppyModsSetActive(bool isActive)
    {
        if (!isActive)
        {
            PoppyModsDestroySimpleImage();
        }
        _poppyModsSimpleImage.enabled = isActive;
        gameObject.SetActive(isActive);
    }
}