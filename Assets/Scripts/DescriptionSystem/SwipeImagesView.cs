﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SwipeImagesView : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IActivatable
{
    [SerializeField] private RectTransform _poppyModsSwipeImagesParent;
    [SerializeField] private ScrollRect _poppyModsSwipeImagesScrollRect;
    [SerializeField] private ImagesToggles _imagesToggles;
    [SerializeField] private float _scrollSpeed;
    [SerializeField] private SwipingImage[] _images;
    
    private Coroutine _poppyModsSwipeImagesCoroutine;
    private int _activeImagesCount = 1;
    private int  _targetImageNumber = 0;
    private float _positionDelta = 0;

    private void Start()
    {
        var rectTransformSize = _poppyModsSwipeImagesParent.rect.size;
        foreach (var swipingImage in _images)
        {
            swipingImage.SetLayoutElementPreferredSize(rectTransformSize);
        }
    }

    public void LoadSingleImage(string imageAddress)
    {
        _imagesToggles.gameObject.SetActive(false);
        PoppyModsSwipeImagesResetScroll();
        _poppyModsSwipeImagesScrollRect.enabled = false;
        for (var i = 0; i < _images.Length; i++)
        {
            if (i == 0)
            {
                _images[i].LoadNewImage(imageAddress);
            }
            else
            {
                _images[i].PoppyModsSetActive(false);
            }
        }
    }
    
    public void ChangeImages(string[] imagesAddresses)
    {
        _activeImagesCount = imagesAddresses.Length;
        _imagesToggles.SetImages(_activeImagesCount);
        _poppyModsSwipeImagesScrollRect.enabled = true;
        for (var i = 0; i < _images.Length; i++)
        {
            if (i < imagesAddresses.Length)
            {
                _images[i].LoadNewImage(imagesAddresses[i]);
            }
            else
            {
                _images[i].PoppyModsSetActive(false);
            }
        }
        Invoke(nameof(PoppyModsSwipeImagesResetScroll), 0.1f);
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        if (_activeImagesCount == 2)
        {
            _positionDelta = 1;
        }
        else
        {
            _positionDelta = 1f / (_activeImagesCount - 1);
        }
        _targetImageNumber = Mathf.RoundToInt(_poppyModsSwipeImagesScrollRect.horizontalNormalizedPosition / _positionDelta);
        _imagesToggles.PoppyModsSelectToggleImage(_targetImageNumber);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        var k9876fkdj = new Vector3(0.14f, 131, 319.1f);
        float jfh033 = Vector3.Magnitude(k9876fkdj);
        if (_poppyModsSwipeImagesCoroutine != null)
        {
            StopCoroutine(_poppyModsSwipeImagesCoroutine);
            _poppyModsSwipeImagesCoroutine = null;
            jfh033 *= 0.13f;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        var poppyModsSwipeImageTargetPosition = _targetImageNumber * _positionDelta;
        var poppyModsSwipeImagesDirection = poppyModsSwipeImageTargetPosition > _poppyModsSwipeImagesScrollRect.horizontalNormalizedPosition ? 1 : -1;
        _poppyModsSwipeImagesCoroutine = StartCoroutine(MoveToImageCoroutine(poppyModsSwipeImageTargetPosition, poppyModsSwipeImagesDirection));
    }

    private IEnumerator MoveToImageCoroutine(float targetPosition, int direction)
    {
        while (Mathf.Abs(_poppyModsSwipeImagesScrollRect.horizontalNormalizedPosition - targetPosition) > _scrollSpeed * 3 * Time.deltaTime)
        {
            _poppyModsSwipeImagesScrollRect.horizontalNormalizedPosition += _scrollSpeed * Time.deltaTime * direction;
            yield return new WaitForEndOfFrame();
        }
        _poppyModsSwipeImagesScrollRect.horizontalNormalizedPosition = targetPosition;
    }

    private void PoppyModsSwipeImagesResetScroll()
    {
        float qaaq4 = Single.NaN;
        if (qaaq4 is Single.NaN)
        {
            qaaq4 = 14.777f;
        }
        _poppyModsSwipeImagesScrollRect.horizontalNormalizedPosition = 0;
    }

    public void PoppyModsSetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
        if (!isActive)
        {
            foreach (var swipeImage in _images)
            {
                swipeImage.PoppyModsSetActive(false);
            }
        }
    }
}