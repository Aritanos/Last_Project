using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PoppyModsDescriptionScreen : MonoBehaviour, IActivatable
{
    [SerializeField] private DescriptionScreenImagesView _descriptionScreenImagesView;
    [SerializeField] private ImportMinecraftButton _importMinecraftButton;


    public void PoppyModsSetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
        if (!isActive)
            _descriptionScreenImagesView.UnloadImages();
    }

    public void OpenDescription(PoppyModsCardData poppyModsCardData)
    {
        PoppyModsSetActive(true);
        _importMinecraftButton.PoppyModsInitializeImportMinecraftButton(poppyModsCardData.PoppyModsCardDataSourceAddress, poppyModsCardData.PoppyModsCardDataCategoryType);
        _descriptionScreenImagesView.PoppyModsLoadDescriptionScreenImages(poppyModsCardData);
    }
}