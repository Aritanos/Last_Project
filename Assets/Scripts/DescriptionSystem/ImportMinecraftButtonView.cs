﻿using TMPro;
using UnityEngine;

public class ImportMinecraftButtonView : MonoBehaviour
{
    [SerializeField] private TMP_Text _poppyModsIstallText;
    [SerializeField] private TMP_Text _poppyModsDownloadingText;
    [SerializeField] private TMP_Text _poppyModsImportDownloadText;

    private TMP_Text _poppyModsMinecraftButtonSelectedText;
    public void SetTextToDownload()
    {
        if (_poppyModsMinecraftButtonSelectedText != null)
        {
            _poppyModsMinecraftButtonSelectedText.gameObject.SetActive(false);
        }
        _poppyModsMinecraftButtonSelectedText = _poppyModsImportDownloadText;
        _poppyModsMinecraftButtonSelectedText.gameObject.SetActive(true);
    }

    public void SetTextToDownloading()
    {
        if (_poppyModsMinecraftButtonSelectedText != null)
        {
            _poppyModsMinecraftButtonSelectedText.gameObject.SetActive(false);
        }
        _poppyModsMinecraftButtonSelectedText = _poppyModsDownloadingText;
        _poppyModsMinecraftButtonSelectedText.gameObject.SetActive(true);
    }

    public void SetTextToInstall()
    {
        if (_poppyModsMinecraftButtonSelectedText != null)
        {
            _poppyModsMinecraftButtonSelectedText.gameObject.SetActive(false);
        }
        _poppyModsMinecraftButtonSelectedText = _poppyModsIstallText;
        _poppyModsMinecraftButtonSelectedText.gameObject.SetActive(true);
    }
}