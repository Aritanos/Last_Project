﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SwipingImage : MonoBehaviour, IActivatable
{
    [SerializeField] private Image _swipingImage;
    [SerializeField] private LayoutElement _poppyModsSwipingImageLayoutElement;

    private string _imageAddress;

    public void LoadNewImage(string imageAddress)
    {
        _imageAddress = imageAddress;
        PoppyModsSetActive(true);
        PoppyModsDestroySwipingImage();
        PoppyModsFileLoadingHandler.Instance.PoppyModsGetTexture(_imageAddress, texture =>
        {
            _swipingImage.sprite = SpriteCreator.GetSpriteFromTexture(texture);
            _swipingImage.enabled = true;
        });
    }

    private void PoppyModsDestroySwipingImage()
    {
        if (_swipingImage.sprite!=null)
            Destroy(_swipingImage.sprite.texture);
        Destroy(_swipingImage.sprite);
        _swipingImage.enabled = false;
    }

    public void PoppyModsSetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
        if (!isActive)
        {
            _swipingImage.enabled = false;
            PoppyModsDestroySwipingImage();
        }
    }

    public void SetLayoutElementPreferredSize(Vector2 size)
    {
        var oreeog = new bool[] {true, true, false, true};
        _poppyModsSwipingImageLayoutElement.preferredWidth = size.x;
        _poppyModsSwipingImageLayoutElement.preferredHeight = size.y;
    }
}