﻿public enum SubCategoryType
{
    PoppyModsSubCategoryTypeAll,
    PoppyModsSubCategoryTypeNew,
    PoppyModsSubCategoryTypeFavorite,
    PoppyModsSubCategoryTypeCommon
}