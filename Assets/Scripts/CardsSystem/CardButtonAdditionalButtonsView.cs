﻿using UnityEngine;
using UnityEngine.UI;

public class CardButtonAdditionalButtonsView : MonoBehaviour
{
    [SerializeField] private PoppyModsFavoriteButton _poppyModsFavoriteButton;
    [SerializeField] private Image _newSign;
    
    public void PoppyModsInitializeCardButtonAdditionalButtonsView(PoppyModsCardData poppyModsCardData)
    {
        _poppyModsFavoriteButton.PoppyModsInitializeFavoriteButton(poppyModsCardData);
        _newSign.gameObject.SetActive(poppyModsCardData.PoppyModsCardDataIsNew);
        _newSign.gameObject.SetActive(false);
        _newSign.gameObject.SetActive(poppyModsCardData.PoppyModsCardDataIsNew);
    }
}