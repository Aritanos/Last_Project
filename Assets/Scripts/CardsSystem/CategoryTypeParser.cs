using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public static class CategoryTypeParser
{
    public static string GetCategoryName(PoppyModsCategoryType categoryType)
    {
        var newName = categoryType switch
        {
            PoppyModsCategoryType.PoppyModsCategoryTypeMaps => StringsDataRepo.ServerTexts.PoppyModsStringsNameMaps,
            PoppyModsCategoryType.PoppyModsCategoryTypeMods => StringsDataRepo.ServerTexts.PoppyModsStringNameMods,
            PoppyModsCategoryType.PoppyModsCategoryTypeSkins => StringsDataRepo.ServerTexts.PoppyModsStringsNameSkins,
            PoppyModsCategoryType.PoppyModsCategoryTypeTextures => StringsDataRepo.ServerTexts.PoppyModsStringsNameTextures,
            _ => ""
        };
        return Regex.Replace(newName, "^[a-z]", m => m.Value.ToUpper());
    }
}
