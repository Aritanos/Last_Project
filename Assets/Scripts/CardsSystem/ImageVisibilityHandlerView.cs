﻿using UnityEngine;

public class ImageVisibilityHandlerView : MonoBehaviour
{
    [SerializeField] private RectTransform _poppyModsVisibilityHandlerRectTransform;
    [SerializeField] private BoxCollider2D _poppyModsImageVisibilityCollider2D;
    
    public void UpdateColliderSize()
    {
        Debug.Log("Handler: " + _poppyModsVisibilityHandlerRectTransform.rect.size);
        _poppyModsImageVisibilityCollider2D.size = _poppyModsVisibilityHandlerRectTransform.rect.size;
    }

    public void PoppyModsImagesVisibilityHandlerSetColliderActive()
    {
        _poppyModsImageVisibilityCollider2D.enabled = true;
    }
}