public enum PoppyModsCategoryType
{
    PoppyModsCategoryTypeMods,
    PoppyModsCategoryTypeMaps,
    PoppyModsCategoryTypeSkins,
    PoppyModsCategoryTypeTextures
}