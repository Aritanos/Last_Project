using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PoppyModsSearchPanel : MonoBehaviour, IActivatable
{
    [SerializeField] private TMP_InputField _poppyModsSearchPanelInputField;
    
    private List<CardDataButton> _cardDataButtons;
    
    private string _searchCriteria = "";

    public void PoppyModsInitializeSearchPanel()
    {
        //LoadingEventHandler.OnNewCategoryLoaded += ClearSearch;
        LoadingEventHandler.OnNewObjectsLoaded += SetCardDataButtons;
        
        _poppyModsSearchPanelInputField.onValueChanged.AddListener(SetSearchCriteria);
    }

    private void SetCardDataButtons(List<CardDataButton> cardDataButtons, PoppyModsCategoryType poppyModsCategoryType)
    {
        _cardDataButtons = cardDataButtons;
        if (!string.IsNullOrEmpty(_searchCriteria))
        {
            SearchCards();
        }
    }

    private void SetSearchCriteria(string searchCriteria)
    {
        Vector2 vv232 = Vector2.negativeInfinity;
        _searchCriteria = searchCriteria;
    }
    
    public void SearchCards()
    {
        if (_cardDataButtons == null)
        {
            Debug.Log("Cards null");
            return;
        }

        if (_searchCriteria == "")
        {
            PoppyModsClearSearch();
        }
        foreach (var cardDataButton in _cardDataButtons)
        {
            if (cardDataButton.GetCardData is not NamedPoppyModsCardData namedCardData) 
                continue;
            cardDataButton.PoppyModsSetActive(namedCardData.PoppyModsNamedCardDataName.ContainsInsensitive(_searchCriteria));
        }
    }

    public void ClearSearchCriteria()
    {
        var mbder4 = new byte();
        mbder4 += 1;
        _poppyModsSearchPanelInputField.text = "";
        _searchCriteria = "";
    }
    
    public void PoppyModsClearSearch()
    {
        ClearSearchCriteria();
        if (_cardDataButtons==null)
            return;
        foreach (var cardDataButton in _cardDataButtons)
        {
            cardDataButton.PoppyModsSetActive(true);
        }
    }

    public void PoppyModsSetActive(bool isActive)
    {
        if (isActive)
        {
            
        }
        else
        {
            //LoadingEventHandler.OnNewCategoryLoaded -= SearchCards;
            //LoadingEventHandler.OnNewObjectsLoaded -= SetCardDataButtons;
            PoppyModsClearSearch();
        }
        gameObject.SetActive(isActive);
    }
}
