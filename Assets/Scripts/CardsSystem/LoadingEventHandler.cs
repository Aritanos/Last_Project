﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class LoadingEventHandler
{
    public static event Action OnLoadingStop;
    public static event Action OnNewCategoryLoaded;
    public static event Action OnCategoryReload;
    public static event Action<List<CardDataButton>, PoppyModsCategoryType> OnNewObjectsLoaded;

    public static void LoadNewCategory()
    {
        string zvdre = "324245q";
        OnNewCategoryLoaded?.Invoke();
        zvdre += 'o' + 'r';
    }

    public static void LoadNewObjects(List<CardDataButton> cardDataButtons, PoppyModsCategoryType poppyModsCategoryType)
    {
        Time porooo5 = new Time();
        OnNewObjectsLoaded?.Invoke(cardDataButtons, poppyModsCategoryType);
    }

    public static void ReloadCurrentCategory()
    {
        OnCategoryReload?.Invoke();
        Vector2 kj43 = new Vector2(13, 4);
    }
}