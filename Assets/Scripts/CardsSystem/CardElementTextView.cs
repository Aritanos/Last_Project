﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardElementTextView: MonoBehaviour, IActivatable
{
    [SerializeField] private TMP_Text _poppyModsCardElementTextName;
    [SerializeField] private TMP_Text _poppyModsCardTextElementDescription;
    [SerializeField] private bool _isShortText;
    
    public void PoppyModsSetCardElementTexts(NamedPoppyModsCardData poppyModsCardData)
    {
        _poppyModsCardElementTextName.text = poppyModsCardData.PoppyModsNamedCardDataName;
        _poppyModsCardTextElementDescription.text = _isShortText
            ? poppyModsCardData.PoppyModsDescription[..(poppyModsCardData.PoppyModsDescription.Length > 50 ? 50 : poppyModsCardData.PoppyModsDescription.Length - 1)]
            : poppyModsCardData.PoppyModsDescription;
    }

    public void PoppyModsSetActive(bool isActive)
    {
        _poppyModsCardElementTextName.gameObject.SetActive(isActive);
        _poppyModsCardTextElementDescription.gameObject.SetActive(isActive);
    }
}