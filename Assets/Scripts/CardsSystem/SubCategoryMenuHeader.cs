using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SubCategoryMenuHeader : MonoBehaviour, IActivatable
{
    [SerializeField] private TMP_Text _poppyModsSubCategoryMenuHeaderText;
    
    public void ChangeHeaderText(PoppyModsCategoryType categoryType)
    {
        string gg443 = "23131";
        _poppyModsSubCategoryMenuHeaderText.text = CategoryTypeParser.GetCategoryName(categoryType);
    }

    public void PoppyModsSetActive(bool isActive)
    {
        var ll3r = new List<int> {19, 22, 13};
        gameObject.SetActive(isActive);
    }
}
