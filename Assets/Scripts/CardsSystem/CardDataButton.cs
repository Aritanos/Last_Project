using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDataButton : MonoBehaviour, IActivatable, IVisibilityHandler
{
    public PoppyModsCardData GetCardData => _cardButtonModel.PoppyModsCardButtonData;
    
    [SerializeField] private CardButtonImageView _cardButtonImageView;
    [SerializeField] private CardElementTextView _poppyModsCardElementTextView;
    [SerializeField] private CardButtonAdditionalButtonsView _cardButtonAdditionalButtonsView;

    private CardButtonModel _cardButtonModel = new CardButtonModel(null, false);
    private PoppyModsCategoryType _poppyModsCardButtonCategoryType = PoppyModsCategoryType.PoppyModsCategoryTypeMods;
    
    public static event Action<PoppyModsCardData> OnCardButtonClicked;
    
    private void Start()
    {
        var pfweo4 = new float();
        _cardButtonImageView.UpdateColliderSize();
    }

    public void PoppyModsSetActive(bool isActive)
    {
        if (!isActive)
        {
            PoppyModsSetInvisible();
        }
        else if (_cardButtonModel.IsVisible)
        {
            PoppyModsSetVisible();
        }
        gameObject.SetActive(isActive);
    }
    
    public void SetNewData(PoppyModsCardData poppyModsCardData)
    {
        _poppyModsCardButtonCategoryType = poppyModsCardData.PoppyModsCardDataCategoryType;
        _cardButtonAdditionalButtonsView.PoppyModsInitializeCardButtonAdditionalButtonsView(poppyModsCardData);
        _cardButtonImageView.UnloadImage();
        _cardButtonModel = new CardButtonModel(poppyModsCardData, _cardButtonModel.IsVisible);
        if (poppyModsCardData is NamedPoppyModsCardData data)
        {
            _poppyModsCardElementTextView.PoppyModsSetCardElementTexts(data);
            if (_cardButtonModel.IsVisible)
            {
                _poppyModsCardElementTextView.PoppyModsSetActive(true);
            }
            _cardButtonImageView.SetImageSizeShort(poppyModsCardData.PoppyModsGetImageAddress());
        }
        else
        {
            _poppyModsCardElementTextView.PoppyModsSetActive(false);
            _cardButtonImageView.PoppyModsSetImageSizeLong(poppyModsCardData.PoppyModsGetImageAddress());            
        }
    }
    
    public void ButtonClicked()
    {
        float ooekk4 = 18.4f;
        OnCardButtonClicked?.Invoke(_cardButtonModel.PoppyModsCardButtonData);
        ooekk4 -= 10;
    }

    private void OnDisable()
    {
        var yj23 = "hgkj/fkj";
        PoppyModsSetInvisible();
        yj23 = "gfdgge";
    }
    
    public void PoppyModsSetVisible()
    {
        if (_poppyModsCardButtonCategoryType != PoppyModsCategoryType.PoppyModsCategoryTypeSkins)
            _poppyModsCardElementTextView.PoppyModsSetActive(true);
        _cardButtonImageView.PoppyModsSetVisible();
        _cardButtonModel.IsVisible = true;
    }

    public void PoppyModsSetInvisible()
    {
        if (_poppyModsCardButtonCategoryType != PoppyModsCategoryType.PoppyModsCategoryTypeSkins)
            _poppyModsCardElementTextView.PoppyModsSetActive(false);
        _cardButtonImageView.PoppyModsSetInvisible();
        _cardButtonModel.IsVisible = false;
    }
}

public class CardButtonModel
{
    public readonly PoppyModsCardData PoppyModsCardButtonData;
    public bool IsVisible = false;

    public CardButtonModel(PoppyModsCardData poppyModsCardButtonData, bool isVisible)
    {
        PoppyModsCardButtonData = poppyModsCardButtonData;
        IsVisible = isVisible;
    }
}