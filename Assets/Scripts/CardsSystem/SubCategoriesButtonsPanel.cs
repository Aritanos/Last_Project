﻿using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SubCategoriesButtonsPanel: MonoBehaviour, IActivatable
{
    [SerializeField] private SubCategoriesButtonsPanelView _subCategoriesButtonsPanelView;
    [SerializeField] private Transform _subCategoryParent;

    private List<PoppyModsSubCategoryButton> _poppyModsSubCategoryButtons = new();
    public void LoadSubCategoryButtons(IEnumerable<string> subCategoriesNames, UnityAction<string> loadingAction)
    {
        RemoveSubCategoryButtons();
        foreach (var subCategoryName in subCategoriesNames)
        {
            var newButton = Spawner.Instance.InstantiateSubCategoryButton(_subCategoryParent);
            newButton.InitButton(loadingAction, subCategoryName);
            _poppyModsSubCategoryButtons.Add(newButton);
            _subCategoriesButtonsPanelView.AddButton(newButton.GetComponent<PoppyModsSubCategoryButtonView>());
        }
        _subCategoriesButtonsPanelView.UpdateButtonsViews(30f);
        //_subCategoriesButtonsPanelView.SetScrollRectValue(0);
    }

    private void RemoveSubCategoryButtons()
    {
        foreach (var button in _poppyModsSubCategoryButtons)
        {
            _subCategoriesButtonsPanelView.RemoveButtonFromList(button.GetComponent<PoppyModsSubCategoryButtonView>());
            button.Destroy();
        }
        _poppyModsSubCategoryButtons.Clear();
    }

    public void PoppyModsSetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }
}