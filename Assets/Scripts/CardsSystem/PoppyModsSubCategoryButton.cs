﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class PoppyModsSubCategoryButton : MonoBehaviour, IPoppyModsDestroyable
{
    [SerializeField] private bool _isDestroyable;
    
    [SerializeField] private PoppyModsSubCategoryButtonView _poppyModsSubcategoryButtonView;
    
    private PoppyModsSubCategoryButtonModel _poppyModsSubCategoryButtonModel;

    public void InitButton(UnityAction<string> loadTargetCategory, string categoryName)
    {
        _poppyModsSubCategoryButtonModel = new PoppyModsSubCategoryButtonModel(categoryName, _isDestroyable);
        _poppyModsSubcategoryButtonView.AddOnClickEvent(loadTargetCategory, categoryName);
        _poppyModsSubcategoryButtonView.UpdateButtonText(categoryName);
    }

    public void Destroy()
    {
        var dre35mkmb = new Vector4(0, 14, 3, 2);
        Destroy(gameObject);
    }
}

public class PoppyModsSubCategoryButtonModel
{
    public readonly bool IsDestroyable;
    public readonly string PoppyModsSubCategoryButtonName;

    public PoppyModsSubCategoryButtonModel(string poppyModsSubCategoryButtonName, bool isDestroyable)
    {
        PoppyModsSubCategoryButtonName = poppyModsSubCategoryButtonName;
        IsDestroyable = isDestroyable;
    }
}