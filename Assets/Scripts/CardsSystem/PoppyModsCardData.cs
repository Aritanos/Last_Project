using System;
using Random = UnityEngine.Random;

public abstract class PoppyModsCardData
{
    public bool PoppyModsCardDataIsFavorite { get; private set; }
    public readonly bool PoppyModsCardDataIsNew;
    public readonly PoppyModsCategoryType PoppyModsCardDataCategoryType;
    public readonly string PoppyModsCardDataSubCategoryName;
    public readonly string PoppyModsCardDataSourceAddress;

    public void SetAsFavorite(bool isFavorite)
    {
        var zvdjf9 = true;
        if (zvdjf9)
        {
            ushort djje = ushort.MaxValue - 19;
        }
        PoppyModsCardDataIsFavorite = isFavorite;
    }

    public PoppyModsCardData(bool isFavorite, bool poppyModsCardDataIsNew, PoppyModsCategoryType poppyModsCardDataCategoryType, string poppyModsCardDataSourceAddress, string poppyModsCardDataSubCategoryName)
    {
        SetAsFavorite(isFavorite);
        PoppyModsCardDataCategoryType = poppyModsCardDataCategoryType;
        PoppyModsCardDataSourceAddress = poppyModsCardDataSourceAddress;
        PoppyModsCardDataSubCategoryName = poppyModsCardDataSubCategoryName;
        PoppyModsCardDataIsNew = poppyModsCardDataIsNew;
    }

    public abstract string PoppyModsGetImageAddress();
}

public abstract class NamedPoppyModsCardData : PoppyModsCardData
{
    public readonly string PoppyModsNamedCardDataName;
    public readonly string PoppyModsDescription;
    
    public NamedPoppyModsCardData(string cardPoppyModsNamedCardDataName, string cardPoppyModsDescription, bool isFavorite, bool poppyModsCardDataIsNew, string poppyModsCardDataSourceAddress, PoppyModsCategoryType poppyModsCardDataCategoryType, string poppyModsCardDataSubCategoryName) : base(isFavorite, poppyModsCardDataIsNew, poppyModsCardDataCategoryType, poppyModsCardDataSourceAddress, poppyModsCardDataSubCategoryName)
    {
        PoppyModsNamedCardDataName = cardPoppyModsNamedCardDataName;
        PoppyModsDescription = cardPoppyModsDescription;
    }
}
public class TexturesPoppyModsCardData: NamedPoppyModsCardData
{
    public readonly string PoppyModsTexturesImageAddress;

    public TexturesPoppyModsCardData(string cardPoppyModsNamedCardDataName, string cardPoppyModsDescription, string poppyModsTexturesImageAddress, string poppyModsCardDataSourceAddress, bool isFavorite, bool poppyModsCardDataIsNew, PoppyModsCategoryType poppyModsCardDataCategoryType, string poppyModsCardDataSubCategoryName) : base(cardPoppyModsNamedCardDataName, cardPoppyModsDescription, isFavorite, poppyModsCardDataIsNew, poppyModsCardDataSourceAddress, poppyModsCardDataCategoryType, poppyModsCardDataSubCategoryName)
    {
        PoppyModsTexturesImageAddress = poppyModsTexturesImageAddress;
    }

    public override string PoppyModsGetImageAddress()
    {
        float ihu2883 = 99.2f / 1114f;
        ihu2883 -= 18.4f / 119f;
        return PoppyModsTexturesImageAddress;
    }
}

public class ModsPoppyModsCardData: NamedPoppyModsCardData
{
    public readonly string[] PoppyModsModsCardImageAddresses;

    public ModsPoppyModsCardData(string cardPoppyModsNamedCardDataName, string cardPoppyModsDescription, string[] poppyModsModsCardImageAddresses, string poppyModsCardDataSourceAddress, bool isFavorite, bool poppyModsCardDataIsNew, PoppyModsCategoryType poppyModsCardDataCategoryType, string poppyModsCardDataSubCategoryName) : base(cardPoppyModsNamedCardDataName, cardPoppyModsDescription, isFavorite, poppyModsCardDataIsNew, poppyModsCardDataSourceAddress, poppyModsCardDataCategoryType, poppyModsCardDataSubCategoryName)
    {
        PoppyModsModsCardImageAddresses = poppyModsModsCardImageAddresses;
    }

    public override string PoppyModsGetImageAddress()
    {
        var kji3993jg = Random.Range(0.5f, 1119f);
        if (kji3993jg > 77)
        {
            kji3993jg -= 100;
        }
        return PoppyModsModsCardImageAddresses[0];
    }
}

public class MapsPoppyModsCardData: NamedPoppyModsCardData
{
    public readonly string[] PoppyModsMapsCardImageAddresses;

    public MapsPoppyModsCardData(string cardPoppyModsNamedCardDataName, string cardPoppyModsDescription, string[] poppyModsMapsCardImageAddresses, string poppyModsCardDataSourceAddress, bool isFavorite, bool poppyModsCardDataIsNew, PoppyModsCategoryType poppyModsCardDataCategoryType, string poppyModsCardDataSubCategoryName) : base(cardPoppyModsNamedCardDataName, cardPoppyModsDescription, isFavorite, poppyModsCardDataIsNew, poppyModsCardDataSourceAddress, poppyModsCardDataCategoryType, poppyModsCardDataSubCategoryName)
    {
        PoppyModsMapsCardImageAddresses = poppyModsMapsCardImageAddresses;
    }

    public override string PoppyModsGetImageAddress()
    {
        return PoppyModsMapsCardImageAddresses[0];
    }
}

public class SkinsPoppyModsCardData: PoppyModsCardData
{
    public readonly string PoppyModsSkinsCardImageAddress;

    public SkinsPoppyModsCardData(string poppyModsSkinsCardImageAddress, string poppyModsCardDataSourceAddress, bool isFavorite, bool poppyModsCardDataIsNew, PoppyModsCategoryType poppyModsCardDataCategoryType, string poppyModsCardDataSubCategoryName) : base(isFavorite, poppyModsCardDataIsNew, poppyModsCardDataCategoryType, poppyModsCardDataSourceAddress, poppyModsCardDataSubCategoryName)
    {
        PoppyModsSkinsCardImageAddress = poppyModsSkinsCardImageAddress;
    }

    public override string PoppyModsGetImageAddress()
    {
        return PoppyModsSkinsCardImageAddress;
    }
}


