﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class handles loading cards
/// </summary>
public class SubCategoryLoader : MonoBehaviour
{
    public static List<CardDataButton> ActiveCardDataButtons { get; private set; }
    public static SubCategoryType CurrentSubCategory { get; private set; }
    
    [SerializeField] private Transform _poppyModsSubCategoryLoaderParentTransform;
    [SerializeField] private GameObject[] _emptyObjects;
    [SerializeField] private ScrollRect _poppyModsSubCategoryLoaderScrollRect;
    [SerializeField] private ImageVisibilityHandler _poppyModsImagesVisibilityHandler;
    
    private List<CardDataButton> _cardDataButtons = new();
    private Coroutine _loadingCoroutine;

    private void Awake()
    {
        FavoritesEventHandler.OnFavoritesChange += UnloadObject;
        LoadingEventHandler.OnLoadingStop += StopLoading;
    }

    public void SpawnCards()
    {
        for (var i = 0; i < 800; i++)
        {
            _cardDataButtons.Add(Spawner.Instance.InstantiateCardDataButton(_poppyModsSubCategoryLoaderParentTransform));
        }
    }

    
    public void LoadNewCategoryObjects(List<PoppyModsCardData> cardDataList, string subCategoryName)
    {
        CurrentSubCategory = SubCategoryType.PoppyModsSubCategoryTypeCommon;
        var newCardDataList = cardDataList.FindAll(cardData => cardData.PoppyModsCardDataSubCategoryName == subCategoryName);
        LoadObjects(newCardDataList, false);
    }

    public void LoadNewCategoryObjects(List<PoppyModsCardData> cardDataList, SubCategoryType subCategoryType)
    {
        CurrentSubCategory = subCategoryType;
        var dataListToLoad = new List<PoppyModsCardData>();
        if (subCategoryType == SubCategoryType.PoppyModsSubCategoryTypeAll)
        {
            dataListToLoad = cardDataList;
            var latestCards = cardDataList.FindAll(item => item.PoppyModsCardDataIsNew);
        }
        else if (subCategoryType == SubCategoryType.PoppyModsSubCategoryTypeFavorite)
        {
            foreach (var card in cardDataList)
            {
                if (card.PoppyModsCardDataIsFavorite)
                    dataListToLoad.Add(card);
            }
        }
        LoadObjects(dataListToLoad, false);
    }

    public void LoadNewCategoryObjects(List<PoppyModsCardData> cardDataList, SubCategoryType subCategoryType, IReadOnlyList<string> subCategoriesNames)
    {
        CurrentSubCategory = subCategoryType;
        var dataListToLoad = new List<PoppyModsCardData>();
        if (subCategoryType != SubCategoryType.PoppyModsSubCategoryTypeNew)
            return;
        foreach (var card in cardDataList)
        {
            if (card.PoppyModsCardDataIsNew)
            {
                dataListToLoad.Add(card);
            }
            
        }
        LoadObjects(dataListToLoad, false);
    }

    private void LoadObjects(List<PoppyModsCardData> cardDataList, bool isReload)
    {
        Resources.UnloadUnusedAssets();
        if (_loadingCoroutine != null)
        {
            StopCoroutine(_loadingCoroutine);
            _loadingCoroutine = null;
        }
        _loadingCoroutine = StartCoroutine(LoadObjectsCoroutine(cardDataList, 100, isReload));
    }

    private IEnumerator LoadObjectsCoroutine(IReadOnlyList<PoppyModsCardData> cardDataList, int objectsPerFrame, bool isReload)
    {
        _poppyModsImagesVisibilityHandler.PoppyModsSetActive(false);
        yield return new WaitForEndOfFrame();
        if (cardDataList == null || isReload)
        {
            UnloadObjects();
        }
        else
        {
            if (cardDataList.Count < _cardDataButtons.Count)
            {
                for (int i = cardDataList.Count; i < _cardDataButtons.Count; i++)
                {
                    _cardDataButtons[i].PoppyModsSetActive(false);
                }
            }
            _poppyModsImagesVisibilityHandler.PoppyModsSetActive(true);
            _poppyModsSubCategoryLoaderScrollRect.verticalNormalizedPosition = 1;
            var index = 0;
            var cardsDataListCount = cardDataList.Count;
            for (var i = 0; i < cardsDataListCount; i++)
            {
                if (i >= _cardDataButtons.Count)
                {
                    var newCardDataButton = Spawner.Instance.InstantiateCardDataButton(_poppyModsSubCategoryLoaderParentTransform);
                    _cardDataButtons.Add(newCardDataButton);
                }
                _cardDataButtons[i].SetNewData(cardDataList[i]);
                _cardDataButtons[i].PoppyModsSetActive(true);
                //.LoadNewObjects(_cardDataButtons.GetRange(0, i+1), _cardDataButtons[0].GetCardData().CategoryType);
                if (++index == objectsPerFrame)
                {

                    foreach (var empty in _emptyObjects)
                    {
                        empty.transform.SetAsLastSibling();
                    }
                    index = 0;
                    LoadingEventHandler.LoadNewObjects(_cardDataButtons.GetRange(0, i+1), _cardDataButtons[0].GetCardData.PoppyModsCardDataCategoryType);
                    yield return new WaitForEndOfFrame();
                    yield return new WaitForEndOfFrame();
                    yield return new WaitForEndOfFrame();

                    _poppyModsImagesVisibilityHandler.PoppyModsImageVisibilityHandlerInitialize();
                }
            }
            LoadingEventHandler.LoadNewObjects(_cardDataButtons.GetRange(0, cardsDataListCount),_cardDataButtons[0].GetCardData.PoppyModsCardDataCategoryType);
        }
        foreach (var empty in _emptyObjects)
        {
            empty.transform.SetAsLastSibling();
        }
    }

    public void UnloadObjects()
    {
        foreach (var cardDataButton in _cardDataButtons)
        {
            cardDataButton.PoppyModsSetActive(false);
            StopLoading();
        }
    }

    public void UnloadObject(PoppyModsCardData poppyModsCardData)
    {
        if (CurrentSubCategory == SubCategoryType.PoppyModsSubCategoryTypeFavorite && !poppyModsCardData.PoppyModsCardDataIsFavorite)
        {
            _cardDataButtons.Find(item=> item.GetCardData.PoppyModsCardDataSourceAddress == poppyModsCardData.PoppyModsCardDataSourceAddress).PoppyModsSetActive(false);
        }
    }

    private void StopLoading()
    {
        if (_loadingCoroutine != null)
        {
            StopCoroutine(_loadingCoroutine);
            _loadingCoroutine = null;
        }
    }
}