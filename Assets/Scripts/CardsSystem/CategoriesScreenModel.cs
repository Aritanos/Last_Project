﻿using System.Collections.Generic;
using UnityEngine;

public class CategoriesScreenModel
{
    public readonly PoppyModsCategoryType PoppyModsCategoryType;

    public List<PoppyModsCardData> CardsDataList => _cardDataList;
    public IReadOnlyList<string> SubCategoriesList => _subCategoriesNames;
    
    private readonly List<string> _subCategoriesNames = new();
    private readonly List<PoppyModsCardData> _cardDataList = new();

    public CategoriesScreenModel(PoppyModsCategoryType currentPoppyModsCategoryType)
    {
        PoppyModsCategoryType = currentPoppyModsCategoryType;
        _cardDataList = CardsDataCreator.GetCardsData(currentPoppyModsCategoryType, out _subCategoriesNames);
        Debug.Log("Get Data: " + _cardDataList[0].PoppyModsCardDataCategoryType);
    }
}