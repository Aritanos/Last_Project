using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Google.MiniJSON;
using UnityEngine;
using JsonConverter;

public static class CardsDataCreator
{
    public static List<PoppyModsCardData> GetCardsData(PoppyModsCategoryType poppyModsCategoryType, out List<string> subCategoriesNames)
    {
        var _poppyModsCardsDataJson = JsonRepository.PoppyModsGetJson;
        
        var newCardsList = new List<PoppyModsCardData>();
        subCategoriesNames = new List<string>();
        switch (poppyModsCategoryType)
        {
            case PoppyModsCategoryType.PoppyModsCategoryTypeMaps:
            {
                foreach (var (subCategoryName, categoriesList) in _poppyModsCardsDataJson.PoppyModsMaps)
                {
                    subCategoriesNames.Add(subCategoryName);
                    newCardsList.AddRange(categoriesList.Values.Select(categoryItem => GetCard(categoryItem, subCategoryName)));
                }

                return newCardsList;
            }
            case PoppyModsCategoryType.PoppyModsCategoryTypeMods:
            {
                foreach (var (subCategoryName, categoriesList) in _poppyModsCardsDataJson.PoppyModsMods)
                {
                    subCategoriesNames.Add(subCategoryName);
                    newCardsList.AddRange(categoriesList.Values.Select(categoryItem => GetCard(categoryItem, subCategoryName)));
                }

                return newCardsList;
            }
            case PoppyModsCategoryType.PoppyModsCategoryTypeTextures:
            {
                foreach (var (subCategoryName, categoriesList) in _poppyModsCardsDataJson.PoppyModsTextures)
                {
                    subCategoriesNames.Add(subCategoryName);
                    newCardsList.AddRange(categoriesList.Values.Select(categoryItem => GetCard(categoryItem, subCategoryName)));
                }

                return newCardsList;
            }
            case PoppyModsCategoryType.PoppyModsCategoryTypeSkins:
            {
                foreach (var (subCategoryName, categoriesList) in _poppyModsCardsDataJson.PoppyModsSkins)
                {
                    subCategoriesNames.Add(subCategoryName);
                    newCardsList.AddRange(categoriesList.Values.Select(categoryItem => GetCard(categoryItem, subCategoryName)));
                }
                return newCardsList;
            }
            default:
                return null;
        }
    }

    private static TexturesPoppyModsCardData GetCard(PoppyModsTexturesItem poppyModsTexturesItem, string subCategoryName)
    {
        return new (poppyModsTexturesItem.PoppyModsCategoryTextureName, poppyModsTexturesItem.PoppyModsTexturesCategoryDescription, poppyModsTexturesItem.PoppyModsTextureCategoryImageAddress,
            poppyModsTexturesItem.SourceAddress, poppyModsTexturesItem.IsInFavorites, poppyModsTexturesItem.IsNew, PoppyModsCategoryType.PoppyModsCategoryTypeTextures, subCategoryName);
    }

    private static ModsPoppyModsCardData GetCard(PoppyModsModsItem poppyModsModsItem, string subCategoryName)
    {
        return new(poppyModsModsItem.PoppyModsModsCategoryName, poppyModsModsItem.PoppyModsModsCategoryDescription, poppyModsModsItem.PoppyModsModsCategoryImageAddress.ToArray(),
            poppyModsModsItem.SourceAddress, poppyModsModsItem.IsInFavorites, poppyModsModsItem.IsNew, PoppyModsCategoryType.PoppyModsCategoryTypeMods, subCategoryName);
    }
    
    private static MapsPoppyModsCardData GetCard(PoppyModsMapsItem poppyModsMapsItem, string subCategoryName)
    {
        return new (poppyModsMapsItem.PoppyModsMapsCategoryName, poppyModsMapsItem.PoppyModsMapsCategoryDescription, poppyModsMapsItem.PoppyModsMapsCategoryImageAddress.ToArray(),
            poppyModsMapsItem.SourceAddress, poppyModsMapsItem.IsInFavorites, poppyModsMapsItem.IsNew, PoppyModsCategoryType.PoppyModsCategoryTypeMaps, subCategoryName);
    }
    
    private static SkinsPoppyModsCardData GetCard(PoppyModsSkinsItem poppyModsSkinsItem, string subCategoryName)
    {
        return new (poppyModsSkinsItem.PoppyModsSkinsCategoryImageAddress,
            poppyModsSkinsItem.SourceAddress, poppyModsSkinsItem.IsInFavorites, poppyModsSkinsItem.IsNew, PoppyModsCategoryType.PoppyModsCategoryTypeSkins, subCategoryName);
    }
}
