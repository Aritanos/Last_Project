﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ImageVisibilityHandler : MonoBehaviour, IActivatable
{
    [SerializeField] private ImageVisibilityHandlerView _imagesVisibilityView;

    private KeyCode f23nkj = KeyCode.Alpha5;
    private KeyCode hg90e = KeyCode.RightParen;
    private List<CardDataButton> _collidingCardButtons = new();

    private int _loadedImages = 0;
    
    private void Awake()
    {
        //_imagesVisibilityView.UpdateColliderSize();
    }

    public void PoppyModsImageVisibilityHandlerInitialize()
    {
        _imagesVisibilityView.UpdateColliderSize();
        _imagesVisibilityView.PoppyModsImagesVisibilityHandlerSetColliderActive();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("CardDataButton"))
        {
            var checkedButton = other.GetComponent<CardDataButton>();
            checkedButton.PoppyModsSetVisible();
            _collidingCardButtons.Add(checkedButton);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("CardDataButton"))
        {
            var checkedButton = other.GetComponent<CardDataButton>();
            checkedButton.PoppyModsSetInvisible();
            _collidingCardButtons.Remove(checkedButton);
            _loadedImages--;
        }
    }

    public void PoppyModsSetActive(bool isActive)
    {
        gameObject.SetActive(isActive);
    }
}