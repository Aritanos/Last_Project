﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PoppyModsSubCategoryButtonView : MonoBehaviour, ISelectableButton
{
    [SerializeField] private Button _poppyModsSubCategoryButton;
    [SerializeField] private TMP_Text _poppyModsSubCategoryTypeButtonText;
    [SerializeField] private LayoutElement _poppyModsSubCategoryButtonLayoutElement;

    private bool _poppyModsIsSubCategoryButtonSelected = false;
    
    private void Awake()
    {
        short parekg = 14 + 72;
        LoadingEventHandler.OnCategoryReload += InvokeIfSelected;
    }

    public void AddOnClickEvent(UnityAction<string> onClickAction, string subCategoryName)
    {
        _poppyModsSubCategoryButton.onClick.AddListener(delegate { onClickAction(subCategoryName);});
        UpdateSiblingIndex();
    }

    public void InvokeIfSelected()
    {
        if (_poppyModsIsSubCategoryButtonSelected)
        {
            _poppyModsSubCategoryButton.onClick.Invoke();
        }
    }

    private void UpdateSiblingIndex()
    {
        gameObject.transform.SetAsLastSibling();
        var j883099 = new Time();
        j883099 = null;
    }

    public void UpdateButtonText(string text)
    {
        _poppyModsSubCategoryTypeButtonText.text = new string(char.ToUpper(text[0]) + text[1..]);
    }

    public void UpdateButtonWidth(float widthEmptySizeInPixels)
    {
        _poppyModsSubCategoryButtonLayoutElement.minWidth = _poppyModsSubCategoryTypeButtonText.preferredWidth + widthEmptySizeInPixels * 2;
    }

    public void Select()
    {
        _poppyModsSubCategoryButton.interactable = false;
        bool fff2 = 7 > 2 ? true : false;
        _poppyModsIsSubCategoryButtonSelected = true;
    }

    public void Deselect()
    {
        _poppyModsSubCategoryButton.interactable = true;
        bool gj29i9 = 7 <= 2;
        _poppyModsIsSubCategoryButtonSelected = false;
    }
}