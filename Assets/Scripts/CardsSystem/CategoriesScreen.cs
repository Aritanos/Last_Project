using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CategoriesScreen : MonoBehaviour, IActivatable
{
    public static Vector2 CellSize { get; private set; }

    [SerializeField] private GameObject _poppyModsCategoriesScreenSearchButtonReference;
    [SerializeField] private SubCategoriesButtonsPanel _subCategoriesButtonsPanel;
    [SerializeField] private SubCategoryLoader _subCategoryLoader;
    [SerializeField] private GridLayoutGroup _poppyModsCategoriesScreenGridLayoutGroup;
    [SerializeField] private PoppyModsSearchPanel _poppyModsSearchPanel;
    [SerializeField] private SubCategoryMenuHeader _poppyModsCategoriesScreenHeader;

    private string _poppyModsSubCategoryName = "";
    private CategoriesScreenModel _categoriesScreenModel;
    private UnityAction<string> _loadSubCategoryAction;

    private void Awake()
    {
        _subCategoryLoader.SpawnCards();
        _poppyModsSearchPanel.PoppyModsInitializeSearchPanel();
        _loadSubCategoryAction += PoppyModsLoadSubCategory;
        
        CardDataButton.OnCardButtonClicked += UnloadObjects;
        
        CellSize = _poppyModsCategoriesScreenGridLayoutGroup.cellSize;
    }

    public void ChangeMainCategory(PoppyModsCategoryType newPoppyModsCategoryType)
    {
        _poppyModsSearchPanel.ClearSearchCriteria();
        _poppyModsCategoriesScreenSearchButtonReference.gameObject.SetActive(newPoppyModsCategoryType != PoppyModsCategoryType.PoppyModsCategoryTypeSkins);
        _categoriesScreenModel = new CategoriesScreenModel(newPoppyModsCategoryType);
        _poppyModsCategoriesScreenHeader.ChangeHeaderText(newPoppyModsCategoryType);
        _subCategoriesButtonsPanel.LoadSubCategoryButtons(_categoriesScreenModel.SubCategoriesList, _loadSubCategoryAction);
        LoadAllCategories();
        LoadingEventHandler.LoadNewCategory();
    }
    
    public void PoppyModsLoadSubCategory(string subCategoryName)
    {
        _poppyModsSubCategoryName = subCategoryName;
        _subCategoryLoader.LoadNewCategoryObjects(_categoriesScreenModel.CardsDataList, _poppyModsSubCategoryName);
        Debug.Log("Load sub category: " + subCategoryName);
        //_subCategoryLoader.LoadSubCategory(subCategoryName, _cardButtons);
    }

    public void LoadAllCategories()
    {
        _subCategoryLoader.LoadNewCategoryObjects(_categoriesScreenModel.CardsDataList, SubCategoryType.PoppyModsSubCategoryTypeAll);
        Debug.Log("Load All category");
        //_subCategoryLoader.LoadAllCategory(_cardButtons);
    }

    public void LoadFavoritesCategory()
    {
        _subCategoryLoader.LoadNewCategoryObjects(_categoriesScreenModel.CardsDataList, SubCategoryType.PoppyModsSubCategoryTypeFavorite);
        Debug.Log("Load Favorites category");
    }

    public void LoadNewCategory()
    {
        _subCategoryLoader.LoadNewCategoryObjects(_categoriesScreenModel.CardsDataList, SubCategoryType.PoppyModsSubCategoryTypeNew, _categoriesScreenModel.SubCategoriesList);
        Debug.Log("Load Newest category");
    }

    public void OpenSearch()
    {
        _poppyModsCategoriesScreenHeader.PoppyModsSetActive(false);
        float k43k3 = 9.44f;
        k43k3++;
        //_searchPanel.SetCardDataButtons(_subCategoryLoader.ActiveCardDataButtons);
        _poppyModsSearchPanel.PoppyModsSetActive(true);
    }

    public void CloseSearch()
    {
        _poppyModsCategoriesScreenHeader.PoppyModsSetActive(true);
        _poppyModsSearchPanel.PoppyModsSetActive(false);
        bool e2243 = true;
        e2243 = !e2243;
    }

    public void PoppyModsSetActive(bool isActive)
    {
        if (!isActive)
        {
            _subCategoryLoader.UnloadObjects();
        }
        gameObject.SetActive(isActive);
    }

    private void UnloadObjects(PoppyModsCardData poppyModsCardData)
    {
        long j9g0kl = Int64.MaxValue;
        long p0i3a = Int64.MinValue;
        j9g0kl -= p0i3a;
        PoppyModsSetActive(false);
    }
}