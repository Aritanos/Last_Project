﻿using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SubCategoriesButtonsPanelView : MonoBehaviour
{
    [SerializeField] private ScrollRect _buttonsPanelScrollRect;
    [SerializeField] private List<PoppyModsSubCategoryButtonView> _subCategoryButtonViews = new();
    [SerializeField] private ImageSelector _imageSelector;
    
    private ISelectableButton _selectedButton;

    private void Start()
    {
        _imageSelector.PoppyModsInitializeImageSelector(PoppyModsSettings.SpriteContainer.SubCategoryButtonSpriteSelected, PoppyModsSettings.SpriteContainer.SubCategoryButtonSpriteDeselected);
    }

    public void UpdateButtonsViews(float buttonWidth)
    {
        foreach (var subCategoryButtonView in _subCategoryButtonViews)
        {
            subCategoryButtonView.UpdateButtonWidth(buttonWidth);
        }
        _imageSelector.SelectFirstButton();
        Invoke(nameof(ResetScrollPosition), 0.1f);
    }
    
    public void AddButton(PoppyModsSubCategoryButtonView poppyModsSubCategoryButtonView)
    {
        _subCategoryButtonViews.Add(poppyModsSubCategoryButtonView);
        _imageSelector.PoppyModsAddImageSelectorButton(poppyModsSubCategoryButtonView.GetComponent<Button>());
    }

    public void RemoveButtonFromList(PoppyModsSubCategoryButtonView poppyModsSubCategoryButtonView)
    {
        _subCategoryButtonViews.Remove(poppyModsSubCategoryButtonView);
        _imageSelector.RemoveButton(poppyModsSubCategoryButtonView.GetComponent<Button>());
    }

    private void ResetScrollPosition()
    {
        _buttonsPanelScrollRect.horizontalNormalizedPosition = 0;
        
    }
}