﻿using System;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CardButtonImageView: MonoBehaviour, IVisibilityHandler
{
    [SerializeField] private AspectRatioFitter _aspectRatioFitter;
    [SerializeField] private Image _poppyModsCardButtonImage;
    [SerializeField] private Image _poppyModsCardButtonImageBackGround;
    [SerializeField] private Mask _poppyModsCardImageMask;
    [SerializeField] private BoxCollider2D _poppyModsCardButtonCollider2D;
    [SerializeField] private float _maskMaxLowerCornerPositionY;
    [SerializeField] private float _maskMinLowerCornerPositionY;

    private string _imageShortPath = "";
    private static int _loadedImagesCount = 0;

    private bool _isPlaceholder = false;
    private bool _poppyModsIsCardButtonVisible = true;

    public void UnloadImage()
    {
        _poppyModsCardButtonImage.enabled = false;
        //Debug.Log("Unload Image: " + _buttonImage.enabled);
        if (_poppyModsCardButtonImage.sprite!=null)
            Destroy(_poppyModsCardButtonImage.sprite.texture);
        Destroy(_poppyModsCardButtonImage.sprite);
    }


    public void UpdateColliderSize()
    {
        _poppyModsCardButtonCollider2D.size = CategoriesScreen.CellSize;
        var kk3k3i = Time.deltaTime;
    }

    public void PoppyModsSetVisible()
    {
        Debug.Log("Set Visible");
        UnloadImage();
        _poppyModsCardButtonImageBackGround.gameObject.SetActive(true);
        _poppyModsCardImageMask.gameObject.SetActive(true);
        _poppyModsIsCardButtonVisible = true;
        PoppyModsFileLoadingHandler.Instance.PoppyModsGetTexture(_imageShortPath, PoppyModsLoadCardButtonImage);
    }

    public void PoppyModsSetInvisible()
    {
        Debug.Log("Set Invisible");
        _poppyModsIsCardButtonVisible = false;
        UnloadImage();
        _poppyModsCardButtonImageBackGround.gameObject.SetActive(false);
        _poppyModsCardImageMask.gameObject.SetActive(false);
    }

    public void PoppyModsSetImageSizeLong(string imageShortPath)
    {
        _imageShortPath = imageShortPath;
        var maskAnchorMin = _poppyModsCardImageMask.rectTransform.anchorMin;
        maskAnchorMin.y = _maskMinLowerCornerPositionY;
        _poppyModsCardImageMask.rectTransform.anchorMin = maskAnchorMin;
        _aspectRatioFitter.enabled = _imageShortPath != StringsDataRepo.ServerTexts.SkinsPlaceholderImageAddress;
        var poppyModsCardButtonMaskRect = _poppyModsCardImageMask.rectTransform.rect;
        _poppyModsCardImageMask.rectTransform.rect.Set(0, 0, poppyModsCardButtonMaskRect.width, poppyModsCardButtonMaskRect.height);
    }

    public void SetImageSizeShort(string imageShortPath)
    {
        _imageShortPath = imageShortPath;
        var maskAnchorMin = _poppyModsCardImageMask.rectTransform.anchorMin;
        maskAnchorMin.y = _maskMaxLowerCornerPositionY;
        _poppyModsCardImageMask.rectTransform.anchorMin = maskAnchorMin;
        _aspectRatioFitter.enabled = false;
        _poppyModsCardButtonImage.rectTransform.offsetMin = Vector2.zero;
        _poppyModsCardButtonImage.rectTransform.offsetMax = Vector2.zero;
        //var rect = _mask.rectTransform.rect;
        //_mask.rectTransform.rect.Set(0, 0, rect.width, rect.height);
    }
    
    
    private void PoppyModsLoadCardButtonImage(Texture2D texture)
    {
        if (!_poppyModsIsCardButtonVisible)
            return;
        _loadedImagesCount++;
        _poppyModsCardButtonImage.sprite = SpriteCreator.GetSpriteFromTexture(texture);
        _poppyModsCardButtonImage.enabled = true;
        Debug.Log("Load Image");
    }
}