﻿public interface IEnableHandler
{
    public void OnDisable();
    public void OnEnable();
}