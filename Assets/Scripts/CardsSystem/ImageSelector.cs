﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageSelector : MonoBehaviour
{
    [SerializeField] private List<Button> _poppyModsImageSelectorButtonsForSelect;
    
    private Sprite _poppyModsImageSelectorSelectedSprite;
    private Sprite _poppyModsImageSelectorDeselectedSprite;
    private Image _poppyModsImageSelectorSelectedImage;

    public void PoppyModsAddImageSelectorButton(Button button)
    {
        _poppyModsImageSelectorButtonsForSelect.Add(button);
        button.onClick.AddListener(delegate { ImageSelectorSelectImage(button.GetComponent<Image>()); });
    }

    public void RemoveButton(Button button)
    {        
        button.onClick.RemoveListener(delegate { ImageSelectorSelectImage(button.GetComponent<Image>()); });
        _poppyModsImageSelectorButtonsForSelect.Remove(button);
    }
    
    public void PoppyModsInitializeImageSelector(Sprite selected, Sprite deselected)
    {
        _poppyModsImageSelectorSelectedSprite = selected;
        _poppyModsImageSelectorDeselectedSprite = deselected;

        foreach (var button in _poppyModsImageSelectorButtonsForSelect)
        {
            button.onClick.AddListener(delegate { ImageSelectorSelectImage(button.GetComponent<Image>()); });
        }
    }
    
    public void SelectFirstButton()
    {
        ImageSelectorSelectImage(_poppyModsImageSelectorButtonsForSelect[0].GetComponent<Image>());
        ChangeTextIfExists(_poppyModsImageSelectorButtonsForSelect[0], true);
    }
    
    private void ImageSelectorSelectImage(Image selectedImage)
    {
        if (_poppyModsImageSelectorSelectedImage != null)
        {
            _poppyModsImageSelectorSelectedImage.sprite = _poppyModsImageSelectorDeselectedSprite;
            _poppyModsImageSelectorSelectedImage.GetComponent<ISelectableButton>()?.Deselect();
            ChangeTextIfExists(_poppyModsImageSelectorSelectedImage, false);
        }

        _poppyModsImageSelectorSelectedImage = selectedImage;
        _poppyModsImageSelectorSelectedImage.sprite = _poppyModsImageSelectorSelectedSprite;
        _poppyModsImageSelectorSelectedImage.GetComponent<ISelectableButton>()?.Select();
        ChangeTextIfExists(selectedImage, true);
    }

    private void ChangeTextIfExists(Component button, bool isBold)
    {
        var poppyModsTextImage = button.GetComponentInChildren<TMP_Text>();
        if (poppyModsTextImage != null)
        {
            poppyModsTextImage.fontStyle = isBold ? FontStyles.Bold : FontStyles.Normal;
        }
    }
}