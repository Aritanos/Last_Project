﻿using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PoppyModsColorSelector: MonoBehaviour
{
    [SerializeField] private List<Button> _poppyModsForMinecraftColorSelectorButtons;
    
    private Color32 _poppyModsSelectedColor;
    private Color32 _poppyModsDeselectedColor;
    private Image _poppyModsColorSelectorSelectedImage;
    
    public void PoppyModsInitializeColorSelector(Color32 selectedColor, Color32 deselectedColor)
    {
        _poppyModsSelectedColor = selectedColor;
        _poppyModsDeselectedColor = deselectedColor;
        foreach (var button in _poppyModsForMinecraftColorSelectorButtons)
        {
            button.onClick.AddListener(delegate { PoppyModsColorSelectorSelectImage(button.GetComponent<Image>()); });
        }
    }

    public void SelectFirstImage()
    {
        PoppyModsColorSelectorSelectImage(_poppyModsForMinecraftColorSelectorButtons[0].GetComponent<Image>());
    }
    
    private void PoppyModsColorSelectorSelectImage(Image selectedImage)
    {
        if (_poppyModsColorSelectorSelectedImage != null)
            _poppyModsColorSelectorSelectedImage.color = _poppyModsDeselectedColor;
        _poppyModsColorSelectorSelectedImage = selectedImage;
        _poppyModsColorSelectorSelectedImage.color = _poppyModsSelectedColor;
    }
}