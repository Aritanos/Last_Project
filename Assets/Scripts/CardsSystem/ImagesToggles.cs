using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImagesToggles : MonoBehaviour
{
    [SerializeField] private Image[] _images;

    private Image _selectedImage;
    public void SetImages(int count)
    {
        gameObject.SetActive(true);
        for (int i = 0; i < _images.Length; i++)
        {
            if (i < count)
            {
                _images[i].gameObject.SetActive(true);
            }
            else
            {
                _images[i].gameObject.SetActive(false);
            }
        }
        ChangeSelectedImage(_images[0]);
    }

    public void PoppyModsSelectToggleImage(int number)
    {
        ChangeSelectedImage(_images[number]);
    }

    private void ChangeSelectedImage(Image image)
    {
        if (_selectedImage != null)
        {
            _selectedImage.sprite = PoppyModsSettings.SpriteContainer.SwiperImageDeselected;
        }
        _selectedImage = image;
        _selectedImage.sprite = PoppyModsSettings.SpriteContainer.SwiperImageSelected;
    }
}