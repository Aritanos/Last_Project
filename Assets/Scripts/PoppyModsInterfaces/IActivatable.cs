﻿public interface IActivatable
{
    public void PoppyModsSetActive(bool isActive);
}