﻿public interface ICardDataGetter
{
    public PoppyModsCardData GetCardData();
}