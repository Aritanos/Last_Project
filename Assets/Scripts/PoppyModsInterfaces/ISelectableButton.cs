using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelectableButton
{
    public void Select();
    public void Deselect();
}
