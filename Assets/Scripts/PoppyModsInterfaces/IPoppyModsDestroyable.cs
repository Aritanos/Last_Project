﻿public interface IPoppyModsDestroyable
{
    public void Destroy();
}