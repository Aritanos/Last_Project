﻿public interface IVisibilityHandler
{
    public void PoppyModsSetVisible();
    public void PoppyModsSetInvisible();
}