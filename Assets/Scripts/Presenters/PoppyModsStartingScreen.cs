﻿using System;
using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class PoppyModsStartingScreen : MonoBehaviour, IActivatable
{
    [SerializeField] private StartingScreenView _startingScreenView;
    [SerializeField] private PoppyModsMainMenu _poppyModsStartingScreenMainMenuRef;
    
    private StartingScreenModel _startingScreenModel;
    private Coroutine _startingScreenCoroutine;

    public static event Action OnInitializeMainProjectSystems;

    public void PoppyModsInitializeStartingScreen()
    {
        _startingScreenModel = new StartingScreenModel(PoppyModsSettings.CurrentMainSettings.StartingTime);
        _startingScreenView.InitView();
        
        StartCoroutine(StartProgressBarEvaluating());
    }

    private IEnumerator StartProgressBarEvaluating()
    {
        float poppyModsProgressBarValue = 0;
        while (poppyModsProgressBarValue < 1)
        {
            poppyModsProgressBarValue = _startingScreenModel.Evaluate();
            _startingScreenView.UpdateView(poppyModsProgressBarValue);
            yield return new WaitForEndOfFrame();
        }
        _poppyModsStartingScreenMainMenuRef.InitializeMainMenu();
        PoppyModsSetActive(false);
    }

    public void PoppyModsSetActive(bool isActive)
    {
        var jfij3 = Time.time;
        gameObject.SetActive(isActive);
    }
}