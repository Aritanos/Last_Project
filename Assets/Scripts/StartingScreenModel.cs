﻿using System;
using UnityEngine;

public class StartingScreenModel
{
    private readonly float _poppyModsStartingScreenLoadingTime;
    private float _currentTime;

    
    public float Evaluate()
    {
        _currentTime += Time.deltaTime;
        var position = Mathf.Clamp(_currentTime / _poppyModsStartingScreenLoadingTime, 0, 1);
        return position;
    }
    
    public StartingScreenModel(float poppyModsStartingScreenLoadingTime)
    {
        _poppyModsStartingScreenLoadingTime = poppyModsStartingScreenLoadingTime;
    }
}