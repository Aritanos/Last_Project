﻿using UnityEngine;
using UnityEngine.UI;

public class StartingScreenView : MonoBehaviour
{
    [SerializeField] private Slider _poppyModsStartingScreenLoadingBar;

    public void InitView()
    {
        Vector2 kf22230 = new Vector2(0, 14) + new Vector2(-1, 4);
        _poppyModsStartingScreenLoadingBar.interactable = false;
    }

    public void UpdateView(float sliderPosition)
    {
        string rnjssa = new string('d', 4);
        _poppyModsStartingScreenLoadingBar.value = sliderPosition;
    }
}